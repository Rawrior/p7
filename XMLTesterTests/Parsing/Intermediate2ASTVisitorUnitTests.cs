﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using XMLTester.AST.Behavioural;
using XMLTester.IntermediateNodes;
using XMLTester.Parsing;
using XMLTester.SemanticAnalysis;

namespace XMLTesterTests.Intermediate2AST.UnitTests
{
    [TestClass]
    public class Intermediate2ASTVisitorUnitTests
    {
        private const string TestFiles = "..\\..\\..\\testUML\\Intermediate2ASTFiles\\";

        [TestClass]
        public class VisitEntityBehaviorNodeMethod
        {
            [TestMethod]
            public void SuccessControllerSingleMachine()
            {
                // Arrange
                StreamReader file = File.OpenText(TestFiles + "EntityBehaviorSuccess.xml");
                Parser parser = new Parser(file);
                parser.Expect(new string[] { "xml", "uml:Model" });
                EntityBehavior node = parser.EntityBehavior();
                Intermediate2ASTVisitor visitor = new Intermediate2ASTVisitor();

                // Act
                var result = visitor.VisitEntityBehaviorNode(node) as 
                    Tuple<IReadOnlyList<CommunicatorNode>, ControllerNode>;

                // Assert
                // The actual contents of the controller and machine node will be tested in
                // other methods. For now, we mostly just want to know they are assimilated
                Assert.IsNotNull(result);
                Assert.IsInstanceOfType(result.Item2, typeof(ControllerNode));
                Assert.IsInstanceOfType(result.Item1, typeof(IReadOnlyList<CommunicatorNode>));
            }
        }

        [TestClass]
        public class VisitMachineNodeMethod
        {

        }

        [TestClass]
        public class VisitControllerNodeMethod
        {

        }

        [TestClass]
        public class VisitSoSNodeMethod
        {

        }

        [TestClass]
        public class VisitComponentDiagramNodeMethod
        {

        }

        [TestClass]
        public class VisitComponentNodeMethod
        {

        }

        [TestClass]
        public class VisitEntityNodeMethod
        {

        }

        [TestClass]
        public class VisitDTONodeMethod
        {

        }

        [TestClass]
        public class VisitAttributeNodeMethod
        {

        }

        [TestClass]
        public class VisitInternalClassNodeMethod
        {

        }

        [TestClass]
        public class VisitClassNodeMethod
        {

        }

        [TestClass]
        public class VisitPropertyNodeMethod
        {

        }

        [TestClass]
        public class VisitOperationNodeMethod
        {

        }

        [TestClass]
        public class VisitConstraintNodeMethod
        {

        }

        [TestClass]
        public class VisitTypeNodeMethod
        {

        }

        [TestClass]
        public class GetTypeFromStringMethod
        {

        }
    }
}
