﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLTester.IntermediateNodes;
using XMLTester.Parsing;

namespace XMLTesterTests.ParserClass.IntegrationTests
{
    [TestClass]
    public class ParserIntegrationTests
    {
        private const string FullParserTestFiles = "..\\..\\..\\TestUML\\FullParserTestFiles\\";

        [TestClass]
        public class ParseMethod
        {
            // TODO: This specific test is probably more an integration test, since it runs through
            // a whole full example.
            [TestMethod]
            public void FullExample()
            {
                // Arrange

                // The file used has been lifted wholesale from how the model looks
                // As of commit 2e3bbbb749ac02dd1a436b99281260833154d3b6

                // This test is simply to ensure that a full file will be parsed.
                // The actual contents of the SoSNode is verified in the SoSNode tests.
                StreamReader file = File.OpenText(FullParserTestFiles + "FullModel.xml");
                Parser parser = new Parser(file);

                // Act
                IntermediateSoSNode SoS = parser.Parse();
                parser.reader.Close();

                // Assert
                Assert.IsNotNull(SoS);
            }

            [TestMethod]
            public void FullExampleFail()
            {
                // Arrange
                StreamReader file = File.OpenText(FullParserTestFiles + "FullModelFail.xml");
                Parser parser = new Parser(file);
                string expectedMessage = "Expected: \"packagedElement\", found: \"FailOnPurpose\", at line 3";

                // Act and assert
                var ex = Assert.ThrowsException<ParseException>(() => parser.Parse());
                Assert.AreEqual(expectedMessage, ex.Message);
            }
        }
    }
}
