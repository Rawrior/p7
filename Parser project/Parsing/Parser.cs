﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using XMLTester.IntermediateNodes;

namespace XMLTester.Parsing
{
    class Parser
    {
        public XmlReader reader;
        public Parser(StreamReader fileStream)
        {
            XmlReaderSettings settings = new XmlReaderSettings { IgnoreWhitespace = true };
            reader = XmlReader.Create(fileStream, settings);
            reader.Read();
        }

        public bool Expect(string expected)
        {
            if (reader.Name != expected)
            {
                throw new ParseException(expected, reader.Name, (reader as IXmlLineInfo).LineNumber);
            }
            return reader.Read();
        }

        public bool Expect(string[] expected)
        {
            bool readOutput = false;
            foreach (string item in expected)
            {
                readOutput = Expect(item);
            }
            return readOutput;
        }

        public IntermediateSoSNode Parse()
        {
            Expect("xml");
            Expect("uml:Model");
            IntermediateSoSNode node = SoSNode();
            Expect("profileApplication");
            Expect("eAnnotations");
            Expect("references");
            Expect("eAnnotations");
            Expect("appliedProfile");
            Expect("profileApplication");
            reader.Close();
            return node;
        }

        public IntermediateSoSNode SoSNode()
        {
            IntermediateSoSNode node = new IntermediateSoSNode();
            while (reader.Name != "uml:Model" && reader.Name != "profileApplication" && reader.AttributeCount != 0) {
                switch (reader.GetAttribute(0))
                {
                    case "uml:Component":
                        if(node.System != null)
                        {
                            throw new ParseException("Registered redundant component diagram declaration at line " + (reader as IXmlLineInfo).LineNumber);
                        }
                        node.System = ComponentDiagramNode();
                        break;
                    case "uml:Package":
                        Expect("packagedElement");
                        node.Dependencies.Add(SoSNode());
                        Expect("packagedElement");
                        break;
                    default:
                        throw new ParseException("Unknown outer element at line " + (reader as IXmlLineInfo).LineNumber);
                }
            }
            return node;
        }

        private EntityNode EntityNode(string name, string count)
        {
            EntityNode entity = new EntityNode(name, count); // Removed ID since the ID gotten here is the wrong one
            Expect("packagedElement");
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                switch (reader.GetAttribute(0))
                {
                    case "uml:Class":
                        entity.DTOs.Add(DTONode());
                        break;
                    case "uml:StateMachine":
                        if(entity.Behavior != null)
                        {
                            throw new ParseException("Registered redundant behavioral definition for " + name + ", at line " + (reader as IXmlLineInfo).LineNumber);
                        }
                        entity.Behavior = EntityBehavior();
                        break;
                    case "uml:Package":
                        if (entity.InternalStructure != null)
                        {
                            throw new ParseException("Registered redundant Internal Class diagram declaration for "+ name + " at line " + (reader as IXmlLineInfo).LineNumber);
                        }
                        entity.InternalStructure = InternalClassNode();
                        break;
                    default:
                        throw new ParseException("Unknown entity feature encountered at " + (reader as IXmlLineInfo).LineNumber);
                }
            }
            Expect("packagedElement");
            return entity;
        }

        public InternalClassNode InternalClassNode()
        { 
            InternalClassNode node = new InternalClassNode();
            Expect("packagedElement");
            while (reader.Name == "packagedElement" && reader.NodeType != XmlNodeType.EndElement)
            {
                switch (reader.GetAttribute(0))
                {
                    case "uml:Class":
                        node.Elements.Add(ClassNode());
                        break;
                    case "uml:Association":
                        node.Elements.Add(AssociationNode());
                        break;
                    default:
                        throw new ParseException("Unknown Package Element encountered at line " + (reader as IXmlLineInfo).LineNumber);
                }
            }
            Expect("packagedElement");
            return node;
        }

        public AssociationNode AssociationNode()
        {
            AssociationNode node = new AssociationNode(
                reader.GetAttribute("xmi:id"),
                reader.GetAttribute("name"),
                reader.GetAttribute("visibility") ?? "public"
            );
            List<string> ends = reader.GetAttribute("memberEnd").Split(' ').ToList();
            if(ends.Count != 2)
            {
                throw new ParseException("AssociationNode requires 2 ends, but found " + ends.Count +" declared, at line " + (reader as IXmlLineInfo).LineNumber);
            }
            Expect("packagedElement");
            Expect("eAnnotations");
            Expect("details");
            Expect("eAnnotations");
            while (reader.Name == "ownedRule")
            {
                node.Constraints.Add(ConstraintNode());
            }
            while (reader.Name == "ownedEnd")
            {
                if (node.ContainingClassID != null && node.ChildClassType != null)
                    throw new ParseException("AssociationNode requires 2 ends, but found " + (ends.Count + 1) + " actual, at line " + (reader as IXmlLineInfo).LineNumber);

                string relatedClassID = reader.GetAttribute("type");
                string aggregation = reader.GetAttribute("aggregation");

                string upper = null;
                string lower = null;

                Expect("ownedEnd");
                if (reader.Name == "lowerValue")
                {
                    lower = reader.GetAttribute("value") ?? "X";
                    Expect("lowerValue");
                }
                if (reader.Name == "upperValue")
                {
                    upper = reader.GetAttribute("value");
                    Expect("upperValue");
                }

                if (aggregation == "composite")
                    node.ChildClassType = new IntermediateTypeNode(relatedClassID, upper, lower);
                else
                    node.ContainingClassID = relatedClassID;

                if (upper != null || lower != null)
                    Expect("ownedEnd");
            }
            Expect("packagedElement");

            if (node.ChildClassType == null)
                throw new ParseException($"ChildClassType is null for association '{node.ID}'");
            if (node.ContainingClassID == null)
                throw new ParseException($"ContainingClassID is null for association '{node.ID}'");

            return node;
        }

        public ClassNode ClassNode()
        {
            ClassNode node = new ClassNode(reader.GetAttribute(1), reader.GetAttribute(2));
            Expect("packagedElement");
            if (reader.Name != "ownedRule"
                && reader.Name != "ownedAttribute"
                && reader.Name != "ownedOperation"
                && reader.Name != "generalization"
                )
                return node;
            while (reader.Name != "packagedElement")
            {
                switch (reader.Name)
                {
                    case "ownedRule":
                        node.Constraints.Add(ConstraintNode());
                        break;
                    case "ownedAttribute":
                        node.Variables.Add(PropertyNode());
                        break;
                    case "ownedOperation":
                        node.Methods.Add(OperationNode());
                        break;
                    case "generalization":
                        node.BaseClassPapID = reader.GetAttribute(2);
                        Expect("generalization");
                        break;
                    default:
                        throw new ParseException("Encountered unknown tag \"" + reader.Name + "\" when parsing class features at line: " + (reader as IXmlLineInfo).LineNumber);
                }
            }
            Expect("packagedElement");
            return node;
        }

        public OperationNode OperationNode()
        {
            string id = reader.GetAttribute("xmi:id");
            string signature = reader.GetAttribute("name");
            string visibility = reader.GetAttribute("visibility");

            OperationNode node = new OperationNode(id, signature, visibility);
            Expect("ownedOperation");
            while (reader.Name == "ownedParameter")
            {
                node.Parameters.Add(ParameterNode());
            }
            if (reader.Name == "ownedOperation" && reader.NodeType == XmlNodeType.EndElement)
                Expect("ownedOperation");
            return node;
        }

        public ParameterNode ParameterNode()
        {
            string id = reader.GetAttribute("xmi:id");
            string signature = reader.GetAttribute("name");
            IntermediateTypeNode type = new IntermediateTypeNode(reader.GetAttribute("type"));
            bool is_return = (reader.GetAttribute("direction") ?? "in") == "out";

            if (!reader.IsEmptyElement)
            {
                Expect("ownedParameter");
                type = TypeNode(type.TypeNameOrID ?? ""); // If the type is null, pass empty string (this is on purpose);
            }
            Expect("ownedParameter");

            return new ParameterNode(signature, id, type, is_return);
        }

        public PropertyNode PropertyNode()
        {
            string id = reader.GetAttribute("xmi:id");
            string signature = reader.GetAttribute("name");
            string visibility = reader.GetAttribute("visibility") ?? "public";
            Expect("ownedAttribute");

            IntermediateTypeNode type = TypeNode("");
            Expect("ownedAttribute");

            return new PropertyNode(id, signature, visibility, type);
        }

        public ConstraintNode ConstraintNode()
        {
            string id = reader.GetAttribute(1);
            string signature = reader.GetAttribute(2);
            Expect("ownedRule");
            Expect("specification");
            Expect("language");
            Expect("");
            Expect("language");
            Expect("body");
            string constraint = reader.Value;
            Expect("");
            Expect("body");
            Expect("specification");
            Expect("ownedRule");
            return new ConstraintNode(id, signature, constraint);
        }

        public EntityBehavior EntityBehavior()
        {
            EntityBehavior machine = new EntityBehavior(reader.GetAttribute(2));
            Expect("packagedElement");
            while (reader.Name == "region")
            {
                BehaviorBaseNode node = RegionNode();
                if (node is ControllerMachineNode)
                {
                    if(machine.controller != null)
                    {
                        throw new ParseException("Multiple controllers defined at " + (reader as IXmlLineInfo).LineNumber);
                    }
                    machine.controller = node as ControllerMachineNode;
                }
                else
                    machine.machines.Add(node as MachineNode);
               
            }
            Expect("packagedElement");
            return machine;
        }

        public MachineNode RegionNode()
        {
            string eventDecl = null;
            MachineNode region = new MachineNode(reader.GetAttribute(2), reader.GetAttribute(1));
            Expect("region");
            while (reader.Name != "region")
            {
                switch (reader.Name)
                {
                    case "transition":
                        region.Transitions.Add(TransitionNode());
                        break;
                    case "subvertex":
                        region.States.Add(StateNode());
                        break;
                    case "ownedComment":
                        if(eventDecl != null)
                        {   
                            throw new ParseException("event declaration of region already set.");
                        }
                        Expect("ownedComment");
                        Expect("body");
                        eventDecl = reader.Value;
                        Expect("");
                        Expect("body");
                        Expect("ownedComment");
                        break;
                    default:
                        throw new ParseException("Line " + (reader as IXmlLineInfo).LineNumber + ": Not transition or state");
                }
            }
            Expect("region");
            if(eventDecl != null)
            {
                return new ControllerMachineNode(region, eventDecl);
            }else 
                return region;
        }

        public StateNode StateNode()
        {
            switch (reader.GetAttribute(0))
            {
                case "uml:Pseudostate":
                    if (reader.AttributeCount == 4 && reader.GetAttribute(3) == "fork")
                    {
                        StateNode fork = new StateNode(reader.GetAttribute(2), reader.GetAttribute(1));
                        Expect("subvertex");
                        return fork;
                    }
                    else
                    {
                        InitialStateNode start = new InitialStateNode(reader.GetAttribute(2), reader.GetAttribute(1));
                        Expect("subvertex");
                        return start;
                    }
                case "uml:FinalState":
                    FinalStateNode end = new FinalStateNode(reader.GetAttribute(2), reader.GetAttribute(1));
                    Expect("subvertex");
                    return end;
                case "uml:State":
                    StateNode state = new StateNode(reader.GetAttribute(2), reader.GetAttribute(1));
                    Expect("subvertex");
                    return state;
                default:
                    throw new ParseException("Line " + (reader as IXmlLineInfo).LineNumber + ": Unknown State Type.");

            }
        }

        public TransitionNode TransitionNode()
        {
            TransitionNode transition = new TransitionNode(reader.GetAttribute(2), reader.GetAttribute(1), reader.GetAttribute(3), reader.GetAttribute(4));
            Expect("transition");
            return transition;
        }

        public IntermediateDTONode DTONode()
        {
            string id = reader.GetAttribute(1);
            string name = reader.GetAttribute(2);
            IntermediateDTONode node = new IntermediateDTONode(name, id);
            Expect("packagedElement");
            while (reader.Name != "packagedElement")
            {
                switch (reader.Name)
                {
                    case "ownedAttribute":
                        node.Fields.Add(AttributeNode());
                        break;
                    case "ownedRule":
                        node.Constraints.Add(ConstraintNode());
                        break;
                    case "generalization":
                        node.BaseDTOID = reader.GetAttribute(2);
                        Expect("generalization");
                        break;
                    default:
                        throw new ParseException("Unknown DTO element encountered at line " + (reader as IXmlLineInfo).LineNumber);
                }
                
            }
            Expect("packagedElement");
            return node;
        }

        private AttributeNode AttributeNode()
        {
            string attributeName = reader.GetAttribute("name");
            string visibility = reader.GetAttribute("visibility");
            string userDefTypeID = reader.GetAttribute("type");
            IntermediateTypeNode type = new IntermediateTypeNode(userDefTypeID);

            if (!reader.IsEmptyElement)
            {
                Expect("ownedAttribute");
                type = TypeNode(userDefTypeID ?? "");
                Expect("ownedAttribute");
            }
            else
                Expect("ownedAttribute");

            return new AttributeNode(attributeName, visibility, type);
        }

        private IntermediateTypeNode TypeNode(string type)
        {
            string upper = null;
            string lower = null;
            if (type == "")
            {
                type = reader.GetAttribute(1);
                Expect("type");
            }
            if (reader.Name == "lowerValue")
            {
                lower = reader.AttributeCount == 3 ? reader.GetAttribute(2) : "X";
                Expect("lowerValue");
            }
            if (reader.Name == "upperValue")
            {
                upper = reader.AttributeCount == 3 ? reader.GetAttribute(2) : "X";
                Expect("upperValue");
            }
            return new IntermediateTypeNode(type, upper, lower);
        }

        public ComponentDiagramNode ComponentDiagramNode()
        {
            ComponentDiagramNode node = new ComponentDiagramNode(reader.GetAttribute("name"));

            bool diagramHasBody = !reader.IsEmptyElement;
            Expect("packagedElement");
            while (reader.Name != "packagedElement" || reader.NodeType != XmlNodeType.EndElement)
            {
                switch (reader.Name)
                {
                    case "ownedAttribute":
                        node.Ports.Add(reader.GetAttribute("xmi:id"));
                        Expect("ownedAttribute");
                        break;
                    case "ownedConnector":
                        node.Links.Add(ConnectorNode());
                        break;
                    case "packagedElement":
                        node.Components.Add(ComponentNode());
                        break;
                    default:
                        throw new ParseException("Unknown component encountered at Line " + (reader as IXmlLineInfo).LineNumber);
                }
            }

            if (diagramHasBody)
                Expect("packagedElement");

            return node;
        }

        private ComponentNode ComponentNode()
        {
            string[] nameAndCount = reader.GetAttribute("name").Split(',');
            ComponentNode node = new ComponentNode(reader.GetAttribute("xmi:id"), nameAndCount[0].Trim(), nameAndCount.ElementAtOrDefault(1)?.Trim());
            if (reader.IsEmptyElement)
            {
                Expect("packagedElement");
                return node;
            }
            Expect("packagedElement");
            while (reader.Name != "packagedElement" || reader.NodeType != XmlNodeType.EndElement)
            {
                switch (reader.GetAttribute(0)) {
                    case "uml:Port": 
                        node.Ports.Add(reader.GetAttribute(1));
                        Expect("ownedAttribute");
                        break;
                    case "uml:Component":
                        node.Constituents.Add(ComponentNode());
                        break;
                    case "uml:Connector":
                        node.Links.Add(ConnectorNode());
                        break;
                    case "uml:Model":
                        node.EntityDecl = EntityNode(node.Name, node.Count);
                        node.EntityDecl.PapID = node.PapID;
                        break;
                    default:
                        throw new ParseException("Unknown component element at " + (reader as IXmlLineInfo).LineNumber);
                }
            }
            Expect("packagedElement");
            return node;
        }

        private ConnectorNode ConnectorNode()
        {
            string name = reader.GetAttribute(2);
            string mult, type;
            if (name != "BE")
            {
                mult = name.Split(',')[0];
                type = name.Split(',')[1];
            }
            else
            {
                mult = "NA";
                type = "NA";
            }
            List<string> ends = new List<string>();
            Expect("ownedConnector");
            ends.Add(reader.GetAttribute(2));
            Expect("end");
            ends.Add(reader.GetAttribute(2));
            Expect("end");
            Expect("ownedConnector");
            return new ConnectorNode(mult, type, ends);
        }
    }
}
