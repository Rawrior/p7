﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using XMLTester.AST;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.AST.Structural;
using XMLTester.AST.Structural.SubSystems;
using XMLTester.SymbolTables;

namespace XMLTester.SemanticAnalysis
{
    partial class Intermediate2ASTVisitor : IIntermediateVisitor<object>
    {
        private readonly Dictionary<string, List<IASTNode>> _papIDToUnfinisheNodes = new();

        private readonly Dictionary<string, string> _papIDToNameMap = new();

        private readonly Dictionary<string, LinkNode> _portIDToLinkNodeMap = new();

        private readonly Dictionary<string, EntityNode> _parentPortIDToBoundaryEntityMap = new();

        private SymbolTable _currentSymbolTable;


        public List<string> Errors { get; } = new List<string>();


        #region behavior
        //We need two pieces of information from this visit: The communicators and controller of the entity.
        public object VisitEntityBehaviorNode(IntermediateNodes.EntityBehavior node)
        {
            //Controller can be handled on its own, without additional processing.
            ControllerNode controller = node.controller.Accept(this) as ControllerNode;
            //We need to separate primary communicator machines from called machines. We do this by finding any machine whose name includes a link-id as well
            List<MachineNode> machines = new List<MachineNode>();
            List<MachineNode> communicatorsMachines = new List<MachineNode>();
            List<CommunicatorNode> communicators = new List<CommunicatorNode>();
            foreach (var machine in node.machines)
            {
                //Visit the machine to get the corresponding AST machine.
                MachineNode mac = machine.Accept(this) as MachineNode;
                //Check for link in naming.
                if (Regex.IsMatch(mac.MachineName, @"\s*[a-zA-Z_][a-zA-Z_0-9]*\s*\[LNK:[a-zA-Z_][a-zA-Z_0-9]*\]\s*$"))
                {
                    communicatorsMachines.Add(mac);
                }
                else
                    machines.Add(mac);
            }
            //Assign each machine to the communicator(s) that make use of them
            foreach (MachineNode mac in communicatorsMachines)
            {
                SymbolTable Scope = StepIntoNewScope(mac.MachineName.Split(" ").First(), ScopeTypes.Communicator);
                List<MachineNode> macs = new List<MachineNode>();
                mac.Scope = StepIntoNewScope(mac.MachineName, ScopeTypes.Machine);
                macs.Add(mac);
                mac.Scope.ASTNode = mac;
                StepOutOfCurrentScope();
                foreach(MachineNode m in machines)
                {
                    if (mac.MachineCalls.Contains(m.MachineName))
                    {
                        mac.Scope = StepIntoNewScope(m.MachineName, ScopeTypes.Machine);
                        macs.Add(m);
                        StepOutOfCurrentScope();
                        continue;
                    }
                }
                //Create the communicator by getting the event trigger-name, the link id and the machines that define the event's behavior.
                var match = Regex.Match(mac.MachineName, @"\s*([a-zA-Z_][a-zA-Z_0-9]*)\s*\[LNK:([a-zA-Z_][a-zA-Z_0-9]*)\]\s*$");
                StepOutOfCurrentScope();
                CommunicatorNode communicator = new CommunicatorNode(match.Groups[1].Value, match.Groups[2].Value, macs);
                Scope.ASTNode = communicator;
                communicator.Scope = Scope;


                communicators.Add(communicator);
            }
            //Return a tuple of the list of communicators and the controller. The entity is defined higher above.
            return new Tuple<IReadOnlyList<CommunicatorNode>, ControllerNode>(communicators, controller);
        }

        public object VisitMachineNode(IntermediateNodes.MachineNode node)
        {
            StepIntoNewScope(node.MachineName, ScopeTypes.Machine);
            //Useful for easily accessing a node.
            Dictionary<string, IntermediateNodes.StateNode> id2Node = new();
            Dictionary<string, string> name2ID = new();
            Dictionary<string, int> intermediates = new();
            foreach (IntermediateNodes.StateNode s in node.States)
            {
                id2Node.Add(s.ID, s);
                name2ID.Add(s.StateName, s.ID);
                intermediates.Add(s.ID, 0);
            }

            //Find all merge states by counting how often a state is a destination. Any above 1 are merges.
            IntermediateNodes.InitialStateNode init = null;
            foreach (IntermediateNodes.TransitionNode tran in node.Transitions)
            {
                intermediates[tran.Destination] += 1;
                if (id2Node[tran.Source] is IntermediateNodes.InitialStateNode source)
                    init = source;
            }

            //Get the set of "roots". This is all states that are either the initial state or merge states.
            var roots = intermediates.Where(kvp => kvp.Value > 1 || kvp.Key == init.ID).Select(x => x.Key);
            //For each root, build the tree.
            List<InvocationGraphTreeRootNode> graphRoots = new List<InvocationGraphTreeRootNode>();
            //First, create the base invokation transitions for use in looping.
            foreach (var rootID in roots)
            {
                List<InvocationTransitionNode> rootInvokations = new List<InvocationTransitionNode>();
                foreach(IntermediateNodes.TransitionNode tran in node.Transitions.Where(t => t.Source == rootID))
                {
                    //Struct is used for easier access and readability. If there were any further use, it should be a class instead.
                    //It divides the "content" of a transition into the various parameters, output and operation.
                    ParsedInvokationTransition pit = new ParsedInvokationTransition(tran.Content);
                    InvocationTransitionNode itn = new InvocationTransitionNode(pit.input, pit.op, pit.output, id2Node[tran.Destination].StateName, new List<InvocationTransitionNode>());
                    rootInvokations.Add(itn);
                }
                //Create a queue that we can loop through for all eternity (or untill we run out of branches to expand)
                Queue<InvocationTransitionNode> queue = new Queue<InvocationTransitionNode>(rootInvokations);
                while(queue.Count > 0)
                {
                    InvocationTransitionNode itn = queue.Dequeue();
                    string itnID = name2ID[itn.EndState];
                    foreach(IntermediateNodes.TransitionNode tran in node.Transitions.Where(t => t.Source == itnID))
                    {
                        ParsedInvokationTransition pit = new ParsedInvokationTransition(tran.Content);
                        InvocationTransitionNode transition = new InvocationTransitionNode(pit.input, pit.op, pit.output, id2Node[tran.Destination].StateName, new List<InvocationTransitionNode>());
                        itn.InvokationBranches.Add(transition);

                        //If a branch ends up in a root state, then we stop, as their behavior are described in other trees.
                        if (roots.Contains(name2ID[transition.EndState]))
                            continue;

                        queue.Enqueue(transition);
                    }
                }
                graphRoots.Add(new InvocationGraphTreeRootNode(id2Node[rootID].StateName, rootInvokations));
            }

            /// TODO: Sort the roots
            StepOutOfCurrentScope();
            return new MachineNode(node.ID, node.MachineName, id2Node.Values.Select(state => state.StateName).ToList(), graphRoots);
        }

        //Converts a controllerNode into its corresponding AST node
        //This conversion is done in two parts: one dedicated to getting the event declarations stored on the node, as well as the machine designating the controller.
        public object VisitControllerNode(IntermediateNodes.ControllerMachineNode node)
        {
            #region eventDecl
            string[] stringEventDecls = node.Content.Replace("&#xD;", "").Replace("\r", "").Split('\n');
            List<EventDeclNode> events = new List<EventDeclNode>();
            foreach (string str in stringEventDecls.Where(line => !string.IsNullOrWhiteSpace(line)).Select(decl => decl.Trim()))
            {
                Match match = Regex.Match(
                    str,
                    @"^\s*(initiator|handler)\s*(\s*[a-zA-Z][a-zA-Z0-9_]*)\s*::\s*((?:[a-zA-Z0-9_()<>][a-zA-Z0-9_()<>]*\s*)*)\s*->\s*((?:[a-zA-Z0-9_()<>][a-zA-Z0-9_()<>]*\s*)*)$"
                );
                //Identify whether the event is for an initiator or handler, and then assign the value thus.
                EventTypes type;
                if (match.Groups[1].Value == "initiator")
                    type = EventTypes.Initiator;
                else if (match.Groups[1].Value == "handler")
                    type = EventTypes.Handler;
                else
                {
                    Errors.Add($"Unknown Event declaration encountered: '{match.Groups[1].Value}' in scope '{_currentSymbolTable.GetFullyQualifiedName()}'");
                    continue;
                }

                string name = match.Groups[2].Value;

                //Divide input into their separate types, and get the TypeNode that corresponds.
                List<TypeNode> input = new List<TypeNode>();
                foreach (string typ in Regex.Split(match.Groups[3].Value.Trim(), @"\s+"))
                {
                    input.Add(GetTypeFromString(typ));
                }
                ////Divide output into their separate types, and get the TypeNode that corresponds.
                List<TypeNode> output = new List<TypeNode>();
                foreach (string typ in Regex.Split(match.Groups[4].Value.Trim(), @"\s+"))
                {
                    output.Add(GetTypeFromString(typ));
                }

                EventDeclNode ev = new EventDeclNode(type, name, input, output);
                events.Add(ev);
            }
            #endregion

            #region Machine
            //We create a dictionary from node ids to the node objects in order to more easily access them, as well as making the code more readable.
            Dictionary<string, IntermediateNodes.StateNode> id2Node = new Dictionary<string, IntermediateNodes.StateNode>();
            Dictionary<string, string> name2ID = new Dictionary<string, string>();
            Dictionary<string, int> occurances = new Dictionary<string, int>();
            foreach (IntermediateNodes.StateNode s in node.States)
            {
                id2Node.Add(s.ID, s);
                name2ID.Add(s.StateName, s.ID);
                occurances.Add(s.ID, 0);
            }

            //We get the number of occurances of each state, where the state is the destination of a transition. Any that occur at least 2 times are mergers.
            IntermediateNodes.InitialStateNode init = null;
            foreach (IntermediateNodes.TransitionNode tran in node.Transitions)
            {
                occurances[tran.Destination] += 1;
                if (id2Node[tran.Source] is IntermediateNodes.InitialStateNode source)
                    init = source;
            }

            //We get a list of all merge states as well as the initial state, as they each will have a tree in the graph.
            List<string> roots = occurances.Where(kvp => kvp.Value > 1 || kvp.Key == init.ID).Select(x => x.Key).ToList();
            List<ActionGraphTreeRootNode> graphRoots = new List<ActionGraphTreeRootNode>();
            foreach (var rootID in roots)
            {
                //Firstly, we create the starting branches of each tree, so that we can loop through them using a queue later.
                List<ActionTransitionNode> rootActions = new List<ActionTransitionNode>();
                foreach (IntermediateNodes.TransitionNode tran in node.Transitions.Where(t => t.Source == rootID))
                {
                    //We use a struct to extract and store the information prior to the construction of the ATN object, as that is both more readable and accessable.
                    //Any further use of said data would most likely necessitate that the struct became a class instead, in order to make the data-parsing more efficient.
                    ParsedActionTransition pat = new ParsedActionTransition(tran.Content);
                    ActionTransitionNode atn = new ActionTransitionNode(pat.ActionName.Replace(":", ""), pat.TimedState, pat.Cmp, pat.Constraints, id2Node[tran.Destination].StateName, new List<ActionTransitionNode>());
                    rootActions.Add(atn);
                }
                //We loop through the queue until no more branches are in it, as that ensures that we construct the entire tree.
                Queue<ActionTransitionNode> queue = new Queue<ActionTransitionNode>(rootActions);
                while (queue.Count > 0)
                {
                    ActionTransitionNode atn = queue.Dequeue();
                    string atnID = name2ID[atn.EndState];
                    List<IntermediateNodes.TransitionNode> transitions = node.Transitions.Where(t => t.Source == atnID).ToList();
                    foreach (IntermediateNodes.TransitionNode tran in transitions)
                    {
                        //As before, we make use of a struct to increase readability and accessability.
                        ParsedActionTransition pat = new ParsedActionTransition(tran.Content);
                        ActionTransitionNode transition = new ActionTransitionNode(pat.ActionName.Replace(":", ""), pat.TimedState, pat.Cmp, pat.Constraints, id2Node[tran.Destination].StateName, new List<ActionTransitionNode>());
                        atn.ActionBranches.Add(transition);

                        //A branch is not added to the queue if it ends in a root state, as those are covered in other trees.
                        if (roots.Contains(name2ID[transition.EndState]))
                            continue;

                        queue.Enqueue(transition);
                    }
                }
                graphRoots.Add(new ActionGraphTreeRootNode(id2Node[rootID].StateName, rootActions));
            }

            /// TODO: Sort the roots
            
            return new ControllerNode(id2Node.Values.Select(node => node.StateName).ToList(), events, graphRoots);
            #endregion
        }


        // All methods below in this region are unused so far.
        public object VisitStateNode(IntermediateNodes.StateNode node)
        {
            throw new NotImplementedException(); // Left unused
        }

        public object VisitTransitionNode(IntermediateNodes.TransitionNode node)
        {
            throw new NotImplementedException(); // Left unused
        }

        public object VisitFinalStateNode(IntermediateNodes.FinalStateNode node)
        {
            throw new NotImplementedException(); // Left unused
        }

        public object VisitInitialStateNode(IntermediateNodes.InitialStateNode node)
        {
            throw new NotImplementedException(); // Left unused
        }

        #endregion behavior

        #region component

        public object VisitSoSNode(IntermediateNodes.IntermediateSoSNode node)
        {
            SymbolTable sosScope = StepIntoNewScope(node.System.Name, ScopeTypes.SoS);

            List<ModelNode> dependencies = node.Dependencies.Select(dep => (ModelNode)dep.Accept(this)).ToList();
            SoSNode mainSoSNode = (SoSNode)node.System.Accept(this);

            ModelNode newModelNode = new ModelNode(mainSoSNode, dependencies, _currentSymbolTable);

            sosScope.ASTNode = newModelNode.MainSoS;
            StepOutOfCurrentScope();

            return newModelNode;
        }

        public object VisitComponentDiagramNode(IntermediateNodes.ComponentDiagramNode node)
        {
            ////////////
            //////////// TODO: parse references
            ////////////

            List<LinkNode> linkNodes = RegisterLinks(node.Components);
            List<SubSystemNode> subSystems =
                node.Components
                .Where(cpnt => !cpnt.Name.StartsWith("LNK:"))
                .Select(component =>
                    (component.EntityDecl is null ? component.Accept(this) : component.EntityDecl.Accept(this)) as SubSystemNode
                ).ToList();

            List<string> boundaryEntityIDs = RegisterLinkConnections(node.Components, subSystems, node.Ports, node.Links);

            SoSNode sosnode = new SoSNode(node.Name, boundaryEntityIDs, subSystems, linkNodes);
            foreach (SubSystemNode sub in subSystems)
            {
                sub.Parent = sosnode;
            }
            return sosnode;
        }

        public object VisitComponentNode(IntermediateNodes.ComponentNode node)
        {
            //// PARHAPS HANDLE REFERENCES?

            StepIntoNewScope(node.Name, ScopeTypes.Constituent);

            List<LinkNode> linkNodes = RegisterLinks(node.Constituents);
            List<SubSystemNode> subSystems =
                node.Constituents
                .Where(cpnt => !cpnt.Name.StartsWith("LNK:"))
                .Select(component =>
                    (component.EntityDecl is null ? component.Accept(this) : component.EntityDecl.Accept(this)) as SubSystemNode
                ).ToList();

            List<string> boundaryEntityIDs = RegisterLinkConnections(node.Constituents, subSystems, node.Ports, node.Links);

            int count = node.Count.ToUpper() switch
            {
                "*" => SubSystemNode.COUNT_UNKNOWN_MANY,
                "N" => SubSystemNode.COUNT_KNOWN_MANY,
                _ => int.Parse(node.Count)
            };

            ConstituentNode consNode = new ConstituentNode(node.PapID, node.Name, count, boundaryEntityIDs, subSystems, linkNodes);
            foreach(SubSystemNode sub in subSystems)
            {
                sub.Parent = consNode;
            }
            _currentSymbolTable.ASTNode = consNode;
            StepOutOfCurrentScope();

            return consNode;
        }

        public object VisitEntityNode(IntermediateNodes.EntityNode node)
        {
            SymbolTable scope = StepIntoNewScope(node.EntityName, ScopeTypes.Entity);

            int count = node.Count?.ToUpper() switch
            {
                "*" => SubSystemNode.COUNT_UNKNOWN_MANY,
                "N" => SubSystemNode.COUNT_KNOWN_MANY,
                null => int.MinValue,
                _ => int.Parse(node.Count)
            };
            if (node.Count == null)
                Errors.Add($"Entity '{scope.GetFullyQualifiedName()}' has no count");

            List<DTONode> dtos = node.DTOs.Select(dto => (DTONode)dto.Accept(this)).ToList();
            List<CLSNode> clss = (List<CLSNode>)node.InternalStructure.Accept(this);
            var comAndCon = (Tuple<IReadOnlyList<CommunicatorNode>, ControllerNode>)node.Behavior.Accept(this);

            foreach (EventDeclNode ev in comAndCon.Item2.EventDeclarations)
                RegisterNewSymbol(ev.Name, ev.EventType == EventTypes.Initiator ? SymbolTypes.Initiator : SymbolTypes.Handler);

            EntityNode entity = new EntityNode(node.PapID, node.EntityName, count, dtos, clss, comAndCon.Item2, comAndCon.Item1);
            entity.Scope = scope;
            scope.ASTNode = entity;
            StepOutOfCurrentScope();

            return entity;
        }

        public object VisitConnectorNode(IntermediateNodes.ConnectorNode node)
        {
            throw new NotImplementedException(); // This is probably gonna be unused
        }


        private List<LinkNode> RegisterLinks(List<IntermediateNodes.ComponentNode> components)
        {
            List<LinkNode> newLinkNodes = new List<LinkNode>();
            foreach (var linkNode in components.Where(cpnt => cpnt.Name.StartsWith("LNK:")))
            {
                string linkName = linkNode.Name.Remove(0, 4);

                Symbol symbol = RegisterNewSymbol(linkName, SymbolTypes.Link);
                LinkNode newLinkNode = new LinkNode(linkName, new List<LinkConnection>());

                linkNode.Ports.ForEach(portID => _portIDToLinkNodeMap.Add(portID, newLinkNode));
                symbol.ASTNode = newLinkNode;
                newLinkNodes.Add(newLinkNode);
            }
            return newLinkNodes;
        }

        private List<string> RegisterLinkConnections(
            IReadOnlyList<IntermediateNodes.ComponentNode> components,
            IReadOnlyList<SubSystemNode> subSystems,
            IReadOnlyList<string> parentPorts,
            IReadOnlyList<IntermediateNodes.ConnectorNode> connectors)
        {
            List<string> boundaryEntityASTids = new List<string>();

            IEnumerable<IntermediateNodes.ConnectorNode> boundaryConnections =
                connectors.Where(link => link.EndIDs.Intersect(parentPorts).Any());
            foreach (var boundaryConnection in boundaryConnections)
            {
                IntermediateNodes.ComponentNode component =
                    components.FirstOrDefault(comp => boundaryConnection.EndIDs.Intersect(comp.Ports).Any());

                if (component == null || component.EntityDecl == null)
                    Errors.Add($"A boundary connection can only lie between a consituent/SoS and an entity (see '{_currentSymbolTable.GetFullyQualifiedName()}')");
                else
                {
                    string parentPort =
                        parentPorts.Contains(boundaryConnection.EndIDs[0])
                        ? boundaryConnection.EndIDs[0]
                        : boundaryConnection.EndIDs[1];

                    string entityPort =
                        parentPorts.Contains(boundaryConnection.EndIDs[0])
                        ? boundaryConnection.EndIDs[1]
                        : boundaryConnection.EndIDs[0];

                    IntermediateNodes.ComponentNode connectedComponent =
                        components.SingleOrDefault(comp => comp.Ports.Contains(entityPort));
                    EntityNode boundaryEntity =
                            subSystems
                            .OfType<EntityNode>()
                            .SingleOrDefault(ent => ent.PapID == connectedComponent.PapID);

                    if (connectedComponent == null)
                        Errors.Add($"A connection from '{_currentSymbolTable.GetFullyQualifiedName()}' is connected to non-child node");
                    else if (boundaryEntity == null)
                        Errors.Add($"No AST entity associated with PapID '{connectedComponent.PapID}' in '{_currentSymbolTable.GetFullyQualifiedName()}'");
                    else
                    {
                        _parentPortIDToBoundaryEntityMap.Add(parentPort, boundaryEntity);
                        boundaryEntityASTids.Add(boundaryEntity.ID);
                    }
                }
            }

            // The opposite of "boundaryConnections". If not a boundary connection, it must be a link connection.
            IEnumerable<IntermediateNodes.ConnectorNode> linkConnections =
                connectors.Where(link => !link.EndIDs.Intersect(parentPorts).Any());
            foreach (var linkConnection in linkConnections)
            {
                string entityOrConstituentPortID;
                LinkNode linkNode;
                if (!_portIDToLinkNodeMap.TryGetValue(linkConnection.EndIDs[0], out linkNode))
                    if (!_portIDToLinkNodeMap.TryGetValue(linkConnection.EndIDs[1], out linkNode))
                    {
                        Errors.Add($"Cannot find AST LinkNode associated with connection between ports '{string.Join(',', linkConnection.EndIDs)}' in '{_currentSymbolTable.GetFullyQualifiedName()}'");
                        continue;
                    }
                    else
                    {
                        entityOrConstituentPortID = linkConnection.EndIDs[0];
                    }
                else
                    entityOrConstituentPortID = linkConnection.EndIDs[1];

                IntermediateNodes.ComponentNode connectedComponent =
                        components.SingleOrDefault(comp => comp.Ports.Contains(entityOrConstituentPortID));
                EntityNode connectedEntity =
                        subSystems
                        .OfType<EntityNode>()
                        .SingleOrDefault(ent => ent.PapID == connectedComponent.PapID);

                if (connectedEntity == null)
                    _parentPortIDToBoundaryEntityMap.TryGetValue(entityOrConstituentPortID, out connectedEntity);

                if (connectedComponent == null)
                    Errors.Add($"A connection from '{_currentSymbolTable.GetFullyQualifiedName()}' is connected to non-child node.");
                else if (connectedEntity == null)
                    Errors.Add($"No AST entity associated with PapID '{connectedComponent.PapID}' in '{_currentSymbolTable.GetFullyQualifiedName()}'");
                else
                {
                    string multStr = linkConnection.Mult.Trim();
                    uint multiplicity = 0;
                    if (multStr != "*")
                    {
                        if (uint.TryParse(multStr, out uint mult))
                            multiplicity = mult;
                        else
                            Errors.Add($"Invalid multiplicity '{linkConnection.Mult}' for entity '{connectedEntity.ID}' in scope '{_currentSymbolTable.GetFullyQualifiedName()}'");
                    }

                    string connectionTypeStr = linkConnection.Type.Trim().ToLower();
                    ConnectionTypes connectionType = ConnectionTypes.Transceiver;
                    if (connectionTypeStr != "transceiver")
                    {
                        if (connectionTypeStr == "transponder")
                            connectionType = ConnectionTypes.Transponder;
                        else
                            Errors.Add($"Invalid link type '{linkConnection.Type}' for entity '{connectedEntity.ID}' in scope '{_currentSymbolTable.GetFullyQualifiedName()}'");
                    }

                    linkNode.Connections.Add(new LinkConnection(connectedEntity.Scope.GetFullyQualifiedName(), multiplicity, connectionType));
                }
            }

            return boundaryEntityASTids;
        }

        #endregion component

        #region DTO

        public object VisitDTONode(IntermediateNodes.IntermediateDTONode node)
        {
            List<Constraint> constraints =
                node.Constraints
                .Select(constraintNode => (Constraint)constraintNode.Accept(this))
                .Concat(
                    node.Fields
                    .Where(field => field.type.LowerMultiplicity != null || field.type.LowerMultiplicity != null)
                    .Select(field => GenerateOCLFromMultiplicities(node.DTOName, field.attributeName, field.type.LowerMultiplicity, field.type.UpperMultiplicity))
                    .Where(ocl => ocl != null)
                ).ToList();

            string baseDTOName = null;
            bool baseDTOFound = node.BaseDTOID != null && TryResolveNameFromPapID(node.BaseDTOID, out baseDTOName);
            DTONode newDTONode = new DTONode(
                node.DTOName,
                baseDTOName,
                node.Fields.Select(attributeNode => (Field)attributeNode.Accept(this)).ToList(),
                constraints
            );
            if (!baseDTOFound && node.BaseDTOID != null)
                RegisterUnfinishedNode(node.BaseDTOID, newDTONode);

            Symbol newSymbol = RegisterNewSymbol(node.DTOName, SymbolTypes.DTO);
            newSymbol.ASTNode = newDTONode;

            string fullyQualifiedName = newSymbol.GetFullyQualifiedName();
            _papIDToNameMap.Add(node.DTOID, fullyQualifiedName);
            HandleUnfinishedNotes(node.DTOID, fullyQualifiedName);

            if (node.DTOID == node.BaseDTOID)
                Errors.Add($"The DTO '{fullyQualifiedName}' it trying to inherit from itself");

            return newDTONode;
        }

        public object VisitAttributeNode(IntermediateNodes.AttributeNode node)
        {
            return new Field((TypeNode)node.type.Accept(this), node.attributeName, "public"); // Always public for DTOs
        }

        #endregion DTO

        #region IC

        public object VisitInternalClassNode(IntermediateNodes.InternalClassNode node)
        {
            List<IntermediateNodes.ClassNode> classNodes = node.Elements.OfType<IntermediateNodes.ClassNode>().ToList();
            List<IntermediateNodes.AssociationNode> assocNodes = node.Elements.OfType<IntermediateNodes.AssociationNode>().ToList();

            // Put relevant associations into each class node
            foreach (var classNode in classNodes)
                classNode.Associations = assocNodes.Where(assoc => assoc.ContainingClassID == classNode.PapID).ToList();

            List<CLSNode> clsNodes = classNodes.Select(classNode => (CLSNode)classNode.Accept(this)).ToList();

            return clsNodes;
        }

        public object VisitClassNode(IntermediateNodes.ClassNode node)
        {
            List<AST.Data.Operation> operations = node.Methods.Select(method => (AST.Data.Operation)method.Accept(this)).ToList();
            List<Field> fields = node.Variables.Select(variable => (Field)variable.Accept(this)).ToList();
            List<Constraint> constraints =
                node.Constraints
                .Select(constraintNode => (Constraint)constraintNode.Accept(this))
                .Concat(
                    node.Variables
                    .Where(field => field.Type.LowerMultiplicity != null || field.Type.LowerMultiplicity != null)
                    .Select(field => GenerateOCLFromMultiplicities(node.ClassName, field.Signature, field.Type.LowerMultiplicity, field.Type.UpperMultiplicity))
                    .Where(ocl => ocl != null)
                ).ToList();

            foreach (IntermediateNodes.AssociationNode assocNode in node.Associations)
            {
                bool isList =
                    !string.IsNullOrEmpty(assocNode.ChildClassType.LowerMultiplicity)
                    || !string.IsNullOrEmpty(assocNode.ChildClassType.UpperMultiplicity);

                constraints.AddRange(node.Constraints.Select(constraintNode => (Constraint)constraintNode.Accept(this)));

                if (isList)
                {
                    Constraint ocl = GenerateOCLFromMultiplicities(
                        node.ClassName,
                        assocNode.Name,
                        assocNode.ChildClassType.LowerMultiplicity,
                        assocNode.ChildClassType.UpperMultiplicity
                    );
                    if (ocl != null)
                        constraints.Add(ocl);

                    fields.Add(new Field(
                        new CollectionTypeNode(CollectionTypes.List, (TypeNode)assocNode.ChildClassType.Accept(this)),
                        assocNode.Name,
                        assocNode.Visibility
                    ));
                }
                else
                {
                    fields.Add(new Field((TypeNode)assocNode.ChildClassType.Accept(this), assocNode.Name, assocNode.Visibility));
                }
            }

            bool baseCLSFound = TryResolveNameFromPapID(node.BaseClassPapID, out string baseCLSNameOrID);
            CLSNode newCLSNode = new CLSNode(
                node.ClassName,
                baseCLSNameOrID,
                fields,
                operations,
                constraints
            );
            if (!baseCLSFound)
                RegisterUnfinishedNode(node.BaseClassPapID, newCLSNode);

            Symbol newSymbol = RegisterNewSymbol(node.ClassName, SymbolTypes.CLS);
            newSymbol.ASTNode = newCLSNode;

            string fullyQualifiedName = newSymbol.GetFullyQualifiedName();
            _papIDToNameMap.Add(node.PapID, fullyQualifiedName);
            HandleUnfinishedNotes(node.PapID, fullyQualifiedName);

            if (node.PapID == node.BaseClassPapID)
                Errors.Add($"The DTO '{fullyQualifiedName}' it trying to inherit from itself");

            return newCLSNode;
        }

        public object VisitPropertyNode(IntermediateNodes.PropertyNode node)
        {
            return new Field((TypeNode)node.Type.Accept(this), node.Signature, node.Visibility);
        }

        public object VisitOperationNode(IntermediateNodes.OperationNode node)
        {
            List<IntermediateNodes.IntermediateTypeNode> outputTypes =
                node.Parameters
                .Where(param => param.IsReturnType)
                .Select(param => param.Type)
                .ToList();
            if (outputTypes.Count != 1)
                Errors.Add($"The operation '{node.Signature}' on a class in entity '{_currentSymbolTable.GetFullyQualifiedName()}' has {outputTypes.Count} output types");

            TypeNode returnType = (TypeNode)outputTypes.FirstOrDefault()?.Accept(this);

            Dictionary<string, TypeNode> inputParameters =
                node.Parameters
                .Where(param => !param.IsReturnType)
                .ToDictionary(param => param.Signature, param => (TypeNode)param.Type.Accept(this));

            return new AST.Data.Operation(returnType, inputParameters, node.Signature, node.Visibility);
        }


        // All methods below in this region are unused so far
        public object VisitParameterNode(IntermediateNodes.ParameterNode node)
        {
            throw new NotImplementedException(); // This is probably gonna be unused
        }

        public object VisitAssociationNode(IntermediateNodes.AssociationNode node)
        {
            throw new NotImplementedException(); // This is probably gonna be unused
        }

        public object VisitConnectionEndNode(IntermediateNodes.ConnectionEndNode node)
        {
            throw new NotImplementedException(); // This is probably gonna be unused
        }

        public object VisitTypedNode(IntermediateNodes.TypedNode node)
        {
            throw new NotImplementedException(); // This is probably gonna be unused
        }

        #endregion IC

        #region IC + DTO

        public object VisitConstraintNode(IntermediateNodes.ConstraintNode node)
        {
            return new Constraint(node.signature, node.constraint);
        }

        public object VisitTypeNode(IntermediateNodes.IntermediateTypeNode node)
        {
            string typeName = node.TypeNameOrID.Split('#').Last();
            TypeNode newTypeNode = typeName switch
            {
                "Boolean" => new PrimitiveTypeNode(PrimitiveTypes.Bool),
                "EByte" => new PrimitiveTypeNode(PrimitiveTypes.Byte),
                "EChar" => new PrimitiveTypeNode(PrimitiveTypes.Char),
                "Integer" => new PrimitiveTypeNode(PrimitiveTypes.Int),
                "Real" => new PrimitiveTypeNode(PrimitiveTypes.Real),
                "String" => new PrimitiveTypeNode(PrimitiveTypes.String),
                _ => null
            };

            // We have a DTO and not a primitive type
            if (newTypeNode == null)
            {
                if (_papIDToNameMap.TryGetValue(typeName, out string fullyQualifiedTypeName))
                    newTypeNode = new UserDefTypeNode(fullyQualifiedTypeName);
                else
                {
                    newTypeNode = new UserDefTypeNode(typeName);
                    RegisterUnfinishedNode(typeName, newTypeNode);
                }
            }

            // If multiplicities are not all null, we have a collection
            if (node.LowerMultiplicity != null || node.UpperMultiplicity != null)
                // Due to the limitations of Papyrus, we dont have a way to represent "set" collections
                newTypeNode = new CollectionTypeNode(CollectionTypes.List, newTypeNode);

            return newTypeNode;
        }

        #endregion


        #region Helpers

        private SymbolTable StepIntoNewScope(string newScopeName, ScopeTypes newScopeType)
        {
            _currentSymbolTable = _currentSymbolTable?.NewScope(newScopeName, newScopeType)
                ?? new SymbolTable(newScopeName, ScopeTypes.SoS);
            return _currentSymbolTable;
        }

        private SymbolTable StepOutOfCurrentScope()
        {
            _currentSymbolTable = _currentSymbolTable.Parent;
            return _currentSymbolTable;
        }


        private Symbol RegisterNewSymbol(string symbolName, SymbolTypes symbolType)
        {
            return _currentSymbolTable.NewSymbol(symbolName, symbolType);
        }


        private bool TryResolveNameFromPapID(string papID, out string name)
        {
            bool found = _papIDToNameMap.TryGetValue(papID, out string match);
            name = match ?? papID;

            return found;
        }


        private void RegisterUnfinishedNode(string papIDofMissingElement, IASTNode astNode)
        {
            if (_papIDToUnfinisheNodes.TryGetValue(papIDofMissingElement, out List<IASTNode> unfinishedNotes))
                unfinishedNotes.Add(astNode);
            else
                _papIDToUnfinisheNodes.Add(papIDofMissingElement, new List<IASTNode>() { astNode });
        }

        private void HandleUnfinishedNotes(string papID, string fullyQualifiedName)
        {
            if (_papIDToUnfinisheNodes.TryGetValue(papID, out List<IASTNode> unfinishedNodes))
            {
                foreach (IASTNode unfinished in unfinishedNodes)
                    switch (unfinished)
                    {
                        case UserDefTypeNode userDef:
                            userDef.UserTypeID = fullyQualifiedName;
                            break;

                        case DTONode dtoNode:
                            dtoNode.Base = fullyQualifiedName;
                            break;

                        case CLSNode clsNode:
                            clsNode.Base = fullyQualifiedName;
                            break;

                        default:
                            throw new InvalidOperationException($"Cannot handle unfinished AST note of type '{unfinished.GetType().FullName}'");
                    }
            }
        }


        private Constraint GenerateOCLFromMultiplicities(string dtoOrClsName, string collectionName, string lower, string upper)
        {
            int upperBound = 0, lowerBound = 0; // 0..*

            if (lower == null && upper == null)
                throw new InvalidOperationException("Cannot make OCL size constraint for 1..1 types");
            else if (lower == "X" && upper == null) // 0..1
                upperBound = 1;
            else if (lower == null && upper == "X") // 1..*
                lowerBound = 1;

            string ocl =

                (lowerBound != 0 ? $"{collectionName}->size() > {lowerBound}" : "") +

                (upperBound != 0 ? $"{collectionName}->size() > {upperBound}" : "");

            return ocl == "" ? null : new Constraint(dtoOrClsName + ":" + collectionName, ocl);
        }

        #endregion
        public static TypeNode GetTypeFromString(string type)
        {
            //We create a dictionary over primitive types in order to make accessing them easier in the future, and to avoid a long chain of if-else statements
            Dictionary<string, PrimitiveTypes> pdt = new Dictionary<string, PrimitiveTypes>
            {
                {"bool", PrimitiveTypes.Bool},
                {"byte", PrimitiveTypes.Byte},
                {"char", PrimitiveTypes.Char},
                {"int", PrimitiveTypes.Int},
                {"real", PrimitiveTypes.Real},
                {"string", PrimitiveTypes.String},
                {"byte-sequence", PrimitiveTypes.ByteSequence},
                {"void", PrimitiveTypes.Void}
            };
            //Check if the type is a primitive type.
            if (pdt.Keys.Contains(type))
                return new PrimitiveTypeNode(pdt[type]);
            //Check if the type is a tuple.
            if(type.ToList().First() == '(' && type.ToList().Last() == ')')
            {
                //Get each individual type-string, and recursively get them. This allows for tuples of tuples of...
                List<string> elements = Regex.Split(type.Skip(1).SkipLast(1).ToString(), @"\s+").ToList();
                List<TypeNode> elementTypes = new List<TypeNode>();
                foreach(string elem in elements)
                {
                    elementTypes.Add(GetTypeFromString(elem));
                }
                return new TupleTypeNode(elementTypes);
            }
            //Check if type is a list or set. First, we have to isolate that part of the string.
            List<string> collec = type.Split('<', 2).ToList();
            if (collec.Count > 1 && (collec[0] == "list" || collec[0] == "set"))
            {
                string elementType = collec[1].Substring(0, collec[1].LastIndexOf('>'));

                CollectionTypes collecType = collec[0] == "list" ? CollectionTypes.List : CollectionTypes.Set;
                TypeNode elemType = GetTypeFromString(elementType);
                return new CollectionTypeNode(collecType, elemType);
            }
            //If nothing else, it must be a user-defined type, such as a DTO or Internal Class.
            return new UserDefTypeNode(type);
        }
        //Struct used for dividing action transition content into its separate components for use in the visitController method.
        public struct ParsedActionTransition
        {
            public string ActionName { get; }
            public string TimedState { get; }
            public Comparators Cmp { get; }
            public List<Tuple<uint, Units>> Constraints { get; }

            public ParsedActionTransition(string content)
            {
                //Get each individual part. Action name and TimedState can be directly set.
                Match match = Regex.Match(
                    content, // Replace since regex cannot handle the colons around handlers
                    @"^\s*(:?[_a-zA-Z][_a-zA-Z0-9]*:?)?\s*\[\s*([_a-zA-Z][_a-zA-Z0-9]*)\s*(<|<=|=|>=|>)\s*([0-9]+\s*(?:d|h|m|s|ms|us)\s*)+\]\s*$");
                ActionName = match.Groups[1].Value;
                TimedState = match.Groups[2].Value;
                string cmp = match.Groups[3].Value;
                //Convert string representation into enum.
                if (cmp == "=")
                    Cmp = Comparators.Eq;
                else if (cmp == "<=")
                    Cmp = Comparators.LessEq;
                else if (cmp == "<")
                    Cmp = Comparators.Less;
                else if (cmp == ">=")
                    Cmp = Comparators.GreaterEq;
                else if (cmp == ">")
                    Cmp = Comparators.Greater;
                else
                    throw new Exception("Unknown comparator " + cmp + " in string: \"" + content + "\"");
                //Get list of timing constraints. This is a list of integer values and time units (stored as a enum). Since the time unit is stored as a string, we have to translate it.
                Constraints = new List<Tuple<uint, Units>>();
                foreach (string constraint in match.Groups[4].Captures.Select(cap => cap.Value.Trim()))
                {
                    Match pair = Regex.Match(constraint, @"^([0-9]+)[ ]*(:d|h|m|s|ms|us)$");
                    Units unit = pair.Groups[2].Value switch
                    {
                        "d" => Units.Day,
                        "h" => Units.Hour,
                        "m" => Units.Minute,
                        "s" => Units.Second,
                        "ms" => Units.Millisecond,
                        "us" => Units.Microsecond,
                        _ => throw new Exception("Unknown unit " + pair.Groups[2].Value),
                    };
                    Constraints.Add(new Tuple<uint, Units>(uint.Parse(pair.Groups[1].Value), unit));
                }
            }
        }

        //This struct is used to parse the content of an invokation transition into its constituent parts, so that they are more accesable in the visit method.
        struct ParsedInvokationTransition
        {
            public List<string> input { get; }
            public List<string> output { get; }
            public AST.Behavioural.Operation op { get; }

            public ParsedInvokationTransition(string content)
            {
                // Check if there is an input or output list. That is: [obj1 obj2 ... objn]
                if (content.StartsWith('['))
                {
                    if (content == "[]") {
                        op = new InputOutputOperation(new List<string> ());
                        input = null;
                        output = null;
                        return;
                    }
                    var ioNames = Regex.Split(content.Trim('[', ']', ' '), @"\s+").ToList();

                    op = new InputOutputOperation(ioNames);
                    input = null;
                    output = null;
                    return;
                }

                //Split the string up into its constituent piececs.
                var match = Regex.Match(
                    content,
                    @"^((?:[a-zA-Z_][a-zA-Z0-9_]*\s*)*)\s*:\s*([a-zA-Z_][a-zA-Z0-9_]*.(?:[0-9]+|\*).[a-zA-Z_][a-zA-Z0-9_]*|[a-zA-Z_][a-zA-Z0-9_]*|[a-zA-Z_][a-zA-Z0-9_]*\s*(?:[a-zA-Z_][a-zA-Z0-9_]*\s*)*)\s*:\s*((?:[a-zA-Z_][a-zA-Z0-9_]*\s*)*)$");
                input = Regex.Split(match.Groups[1].Value, @"\s+").Where(str => str != "").ToList();
                output = Regex.Split(match.Groups[3].Value, @"\s+").Where(str => str != "").ToList();
                //The invokation is a handler, and we specify which entity it is on.
                if (Regex.IsMatch(match.Groups[2].Value, @"[a-zA-Z_][a-zA-Z0-9_]*.(?:[0-9]+|\*).[a-zA-Z_][a-zA-Z0-9_]*"))
                {
                    op = new InvokeEventOperation(match.Groups[2].Value);
                }
                //The invokation is a function, which is also defined here.
                else if (Regex.IsMatch(match.Groups[2].Value, @"[a-zA-Z_][a-zA-Z0-9_]*\s*(?:[a-zA-Z_][a-zA-Z0-9_]*\s*)*"))
                {
                    List<TypeNode> parameterTypes = new List<TypeNode>();
                    List<string> split = Regex.Split(match.Groups[2].Value, @"\s+").ToList();
                    //We get the type definition of the function.
                    foreach (string param in split.Skip(1))
                    {
                        parameterTypes.Add(GetTypeFromString(param));
                    }
                    op = new InvokeFunctionOperation(split[0], parameterTypes);
                }
                //The invokation is a call to another machine.
                else if(Regex.IsMatch(match.Groups[2].Value, @"[a-zA-Z_][a-zA-Z0-9_]*"))
                {
                    op = new InvokeMachineOperation(match.Groups[2].Value);
                }
                else
                    throw new Exception("Could not identify invokation");
            }
        }
    }

}
