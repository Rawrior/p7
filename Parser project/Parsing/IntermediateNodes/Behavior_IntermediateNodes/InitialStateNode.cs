﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XMLTester.IntermediateNodes
{
    public class InitialStateNode : StateNode
    {
        public InitialStateNode(string stateName, string id) : base(stateName, id)
        {
        }
    }
}
