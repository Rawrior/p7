﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.IntermediateNodes;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public abstract class BehaviorBaseNode : INode
    {
        public abstract T Accept<T>(IIntermediateVisitor<T> visitor);
    }
}
