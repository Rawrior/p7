﻿using System;
using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class EntityBehavior : BehaviorBaseNode
    {
        public string entityName; //Might be unnecessary. future will tell.
        public List<MachineNode> machines;
        public ControllerMachineNode controller = null;

        public EntityBehavior(string entityName)
        {
            this.entityName = entityName;
            machines = new List<MachineNode>();
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitEntityBehaviorNode(this);
        }
    }
}
