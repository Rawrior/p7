﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class IntermediateTypeNode : DTOBaseNode
    {
        public string TypeNameOrID { get; }
        public string UpperMultiplicity { get; }
        public string LowerMultiplicity { get; }

        public IntermediateTypeNode(string typeNameOrID, string upper = null, string lower = null)
        {
            TypeNameOrID = typeNameOrID;
            UpperMultiplicity = upper;
            LowerMultiplicity = lower;
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitTypeNode(this);
        }
    }
}
