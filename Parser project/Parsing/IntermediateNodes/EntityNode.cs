﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.IntermediateNodes;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class EntityNode : INode
    {
        public EntityBehavior Behavior { get; set; }
        public InternalClassNode InternalStructure { get; set; }
        public List<IntermediateDTONode> DTOs { get; }

        public string EntityName { get; set; }
        public string PapID { get; set; }
        public string Count { get; }


        public EntityNode(string entityName, string count)
        {
            EntityName = entityName;
            Count = count;

            DTOs = new List<IntermediateDTONode>();
        }


        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitEntityNode(this);
        }
    }
}
