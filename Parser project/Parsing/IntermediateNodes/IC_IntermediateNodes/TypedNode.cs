using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class TypedNode : INode
    {
        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitTypedNode(this);
        }
    }
}