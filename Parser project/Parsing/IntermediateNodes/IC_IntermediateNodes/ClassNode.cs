﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class ClassNode : PackagedElementNode, INode
    {
        // Papyrus ID
        public string PapID { get; }

        public string ClassName { get; }
        public string BaseClassPapID { get; set; }

        public List<PropertyNode> Variables { get; }
        public List<AssociationNode> Associations { get; set; }

        public List<OperationNode> Methods { get; }
        public List<ConstraintNode> Constraints { get; }


        public ClassNode(string _id, string _name)
        {
            PapID = _id;
            ClassName = _name;
            BaseClassPapID = "";
            Variables = new List<PropertyNode>();
            Methods = new List<OperationNode>();
            Constraints = new List<ConstraintNode>();
        }

        public override T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitClassNode(this);
        }
    }
}
