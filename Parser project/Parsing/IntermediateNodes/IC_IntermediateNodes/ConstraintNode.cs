﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.SemanticAnalysis;

namespace XMLTester.IntermediateNodes
{
    public class ConstraintNode : INode
    {
        public string signature;

        protected internal string ID { get; }

        public string constraint;

        public ConstraintNode(string _id, string _sig, string _constraint)
        {
            ID = _id;
            signature = _sig;
            constraint = _constraint;
        }

        public T Accept<T>(IIntermediateVisitor<T> visitor)
        {
            return visitor.VisitConstraintNode(this);
        }
    }
}
