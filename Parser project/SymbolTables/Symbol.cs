﻿using System;
using System.Collections.Generic;
using XMLTester.AST;
using XMLTester.Exceptions;

namespace XMLTester.SymbolTables
{
    public class Symbol
    {
        public string Name { get; }

        public SymbolTable Parent { get; }

        public SymbolTypes SymbolType { get; }

        public IASTNode ASTNode { get; set; }

        public Dictionary<string, object> Attributes { get; } = new Dictionary<string, object>();
        

        public Symbol(string name, SymbolTable parent, SymbolTypes symbolType)
        {
            Name = name ?? throw new ArgumentException(nameof(name));
            Parent = parent ?? throw new ArgumentException(nameof(parent));
            SymbolType = symbolType;
        }


        public string GetFullyQualifiedName()
        {
            return Parent.GetFullyQualifiedName() + '.' + Name;
        }

        public List<string> Validate()
        {
            // TODO: CHECK TO SEE IF FINISHED LATER
            List<string> errors = new List<string>();

            if (ASTNode == null)
                errors.Add($"The symbol '{GetFullyQualifiedName()}' has no associated AST node");

            return errors;
        }
    }

    public enum SymbolTypes
    {
        Initiator, Handler, Link, DTO, CLS, Function, Variable
    }
}
