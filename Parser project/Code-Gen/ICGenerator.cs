﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;

namespace XMLTester.Code_Gen
{
    class ICGenerator
    {
        public void generate(CLSNode internalClass, string destination)
        {
            string path = "..//..//out//" + destination;
            Directory.CreateDirectory(path);
            StreamWriter writer = new StreamWriter(File.Create(path + "//" + internalClass.ID + ".h"));
            includes = new HashSet<string> { "<stdlib.h>" };
            string declaration = GenerateDeclaration(internalClass);
            string incl = "";
            foreach(string inc in includes)
            {
                if(inc.First() == '<')
                {
                    incl += "#include " + inc + "\n";
                }
                else
                {
                    incl += "#include \"" + inc + "\"\n";
                }
            }

            string res = "#ifndef " + destination.Replace("//", "_") + "_" + internalClass.ID + "_H\n" +
                "#define " + destination.Replace("//", "_") + "_" + internalClass.ID + "_H\n\n" + incl + "\n" + declaration + "\n#endif";
            writer.Write(res);
            writer.Flush();
            writer.Close();
        }

        private string GenerateDeclaration(CLSNode internalClass)
        {
            string res = "class " + internalClass.ID;
            if(internalClass.Base != "")
            {
                res += " : " + internalClass.Base;
            }
            res += "{\n";
            Indent();
            foreach(Constraint ocl in internalClass.OCLs)//TODO better OCL handling at gen time.
            {
                res += GetTabDepth() + "//" + ocl.Value;
            }
            if(internalClass.Operations.Where(o => o.Visibility == "public").Any() || internalClass.Fields.Where(f => f.Visibility == "public").Any())
            {
                res += GetTabDepth() + "public:\n";
                Indent();
                foreach (Field field in internalClass.Fields.Where(f => f.Visibility == "public"))
                {
                    string type = GetCPPType(field.Type);
                    res += GetTabDepth() + type + " ";
                    if (field.Name == null) {
                        res += type.ToLower() + ";\n"; 
                    }
                    else
                    {
                        res += field.Name + ";\n";
                    }
                }
                res += "\n";
                foreach(Operation op in internalClass.Operations.Where(o => o.Visibility == "public"))
                {
                    res += GetTabDepth() + GetCPPType(op.ReturnType) + " " + op.Name + "(";
                    foreach(var input in op.InputParameters)
                    {
                        res += GetCPPType(input.Value) + " " + input.Key + ", ";
                    }
                    res = res.Remove(res.Length - 2) + "){\n";
                    Indent();
                    res += GetTabDepth() + "//TODO implement function\n";
                    Unindent();
                    res += "}\n";
                }
                Unindent();
            }
            res += "\n";
            if (internalClass.Operations.Where(o => o.Visibility != "public").ToList().Count > 0 || internalClass.Fields.Where(f => f.Visibility != "public").ToList().Count > 0)
            {
                res += GetTabDepth() + "private:\n";
                Indent();
                foreach (Field field in internalClass.Fields.Where(f => f.Visibility != "public"))
                {
                    res += GetTabDepth() + GetCPPType(field.Type) + " " + field.Name + ";\n";
                }
                res += "\n";
                foreach (Operation op in internalClass.Operations.Where(o => o.Visibility != "public"))
                {
                    res += GetTabDepth() + GetCPPType(op.ReturnType) + " " + op.Name + "(";
                    foreach (var input in op.InputParameters)
                    {
                        res += GetCPPType(input.Value) + " " + input.Key + ", ";
                    }
                    res = res.Remove(res.Length - 2) + "){\n";
                    Indent();
                    res += GetTabDepth() + "//TODO implement function\n";
                    Unindent();
                    res += "}\n";
                }
                Unindent();
            }
            Unindent();
            res += "};";
            return res;
        }

        private string GetCPPType(TypeNode type)
        {
            if (type is CollectionTypeNode collec)
            {
                if (collec.CollectionType == CollectionTypes.List)
                {
                    includes.Add("<vector>");
                    return "std::vector<" + GetCPPType(collec.ElementType) + ">";
                }
                else if (collec.CollectionType == CollectionTypes.Set)
                {
                    includes.Add("<set>");
                    return "std::set<" + GetCPPType(collec.ElementType) + ">";
                }
                else
                    throw new Exception("Unknown collection type " + collec.CollectionType + " encountered during API gen");
            }
            else if (type is PrimitiveTypeNode prim)
            {
                switch (prim.Type)
                {
                    case PrimitiveTypes.Bool:
                        return "bool";
                    case PrimitiveTypes.Byte:
                        {
                            includes.Add("<cstdint>");
                            return "std::uint_8";
                        }
                    case PrimitiveTypes.Char:
                            return "char";
                    case PrimitiveTypes.Int:
                        return "int";
                    case PrimitiveTypes.Real:
                        return "double";
                    case PrimitiveTypes.String:
                        {
                            includes.Add("<string>");
                            return "std::string";
                        }
                    case PrimitiveTypes.ByteSequence:
                        {
                            includes.Add("<vector>");
                            includes.Add("<<cstdint>>");
                            return "std::vector<std::uint_8>";
                        }
                    case PrimitiveTypes.Void:
                            return "void";
                    default:
                        throw new Exception("Encountered unknown primitive type " + prim.Type + " during API gen");
                }
            }
            else if (type is TupleTypeNode tup)
            {
                string res = "std::tuple<";
                foreach(TypeNode elemtype in tup.ElementTypes)
                {
                    includes.Add("<tuple>");
                    res += GetCPPType(elemtype) + ", ";
                }
                res = res.Remove(res.Length - 2) + ">";
                return res;
            }
            else if (type is UserDefTypeNode usrdef)
            {
                includes.Add(usrdef.UserTypeID.Split(".").Last() + ".h");
                int index = usrdef.UserTypeID.LastIndexOf(".");
                string res = usrdef.UserTypeID.Substring(index, usrdef.UserTypeID.Length - index);
                return res.Substring(1, res.Length - 1);
            }
            else
                throw new Exception("Unknown type encountered at API generation.");
        }

        private int tabDepth = 0;

        private void Indent()
        {
            ++tabDepth;
        }

        private void Unindent()
        {
            if(tabDepth == 0)
            {
                throw new Exception("Tab depth cannot be decreased below 0");
            }
            --tabDepth;
        }

        private string GetTabDepth()
        {
            string res = "";
            for(int i = 0; i < tabDepth; ++i)
            {
                res += "\t";
            }
            return res;
        }

        private HashSet<string> includes;

    }
}
