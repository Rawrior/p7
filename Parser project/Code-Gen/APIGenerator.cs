﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using XMLTester.AST;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.AST.Structural.SubSystems;
using XMLTester.SymbolTables;

namespace XMLTester.Code_Gen
{
    class APIGenerator
    {
        private string OpenAPIPath { get; }
        public APIGenerator(string _openAPIPath)
        {
            OpenAPIPath = _openAPIPath;
        }
        public List<string> Generate(EntityNode entity, string destination) {
            Dictionary<string, Tuple<IReadOnlyList<Symbol>, IReadOnlyList<Symbol>>> handlers = new Dictionary<string, Tuple<IReadOnlyList<Symbol>, IReadOnlyList<Symbol>>>();
            List<string> errors = new();
            #region prelude
            foreach(CommunicatorNode com in entity.Communicators)
            {
                foreach(MachineNode mac in com.Machines)
                {
                    foreach(var h in mac.HandlerCallsWithVariables)
                    {
                        List<Symbol> inputs = new List<Symbol>();
                        foreach(string input in h.Value.input)
                        {
                            Symbol inp = mac.Scope.GetSymbol(input);
                            if (inp == null)
                            {
                                throw new Exception("input " + input + " give null");
                            }
                            inputs.Add(inp);
                        }
                        List<Symbol> outputs = new List<Symbol>();
                        foreach (string output in h.Value.output)
                        {
                            Symbol inp = mac.Scope.GetSymbol(output);
                            if (inp == null)
                            {
                                throw new Exception("output " + outputs + " give null");
                            }
                            inputs.Add(inp);
                        }
                        handlers.Add(h.Key, new Tuple<IReadOnlyList<Symbol>, IReadOnlyList<Symbol>> (inputs, outputs));
                    }
                }
            }
            #endregion
            if (handlers.Count > 0)
            {

                errors.AddRange(GenerateClient(entity, handlers, destination));
            }
            if(entity.Controller.EventDeclarations.Where(x=> x.EventType == EventTypes.Handler).Count() > 0)
            {
                errors.AddRange(GenerateServer(entity, destination));
            }
            return errors;
        }

        private List<string> GenerateClient(EntityNode entity,
            Dictionary<string, Tuple<IReadOnlyList<Symbol>, IReadOnlyList<Symbol>>> handlers, string destination)
        {
            List<string> errors = new();
            string directory = "../../out/" + destination + "/OpenApi/Client";
            Directory.CreateDirectory(directory);
            using (StreamWriter writer = new StreamWriter(File.Create("../../out/" + destination.Replace("//", "/") + "/OpenApi/Client/specClient.yaml")))
            {
                //Write front-matter
                writer.Write(
                    "openapi: 3.0.0\n" +
                    "info:\n" +
                    "  description: >-\n" +
                    "    This is an auto-generated API-component for the " + entity.ID + " entity.\n" +
                    "  version: 1.0.0\n" +
                    "  title: " + entity.ID + "\n");
                writer.Flush();
                var dtos = entity.DTOs.Concat(entity.Scope.PropagatedSymbols.Select(x => x.Symbol.ASTNode as DTONode)).ToList();
                //Write calls/paths
                writer.Write("paths:\n");
                Indent();
                foreach (var x in handlers)
                {
                    writer.Write(GetPathsForClient(x));
                }
                writer.Flush();
                //Write components
                if (dtos.Count > 0)
                {
                    string components = "components:\n";
                    Indent();
                    components += GetTabDepth() + "schemas:\n";
                    Indent();
                    writer.Write(components);
                    foreach (DTONode dto in dtos)
                    {
                        writer.Write(GetSchemaForDTO(dto));
                    }
                    unindent();
                    unindent();
                    writer.Flush();
                }
            }
            Process validator = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = @"java",
                    Arguments = "-jar " + OpenAPIPath + " validate -i ../../out/" + destination.Replace("//", "/") + "/OpenApi/Client/specClient.yaml",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            validator.Start();
            while (!validator.StandardOutput.EndOfStream)
            {
                errors.Add(validator.StandardOutput.ReadLine());
            }
            validator.WaitForExit();
            if (errors.Count == 2 && errors[1] == "No validation issues detected.")
                errors.RemoveRange(0, 2);
            if (errors.Any())
            {
                return errors.Skip(1).ToList();
            }
            Process proc = Process.Start(@"java", "-jar " + OpenAPIPath + " generate -i ../../out/" + destination.Replace("//", "/") + "/OpenApi/Client/specClient.yaml -g cpp-restsdk -o " + directory.Replace("//", "/"));
            proc.WaitForExit();
            proc.Close();
            proc.Dispose();
            return errors;
        }

        private List<string> GenerateServer(EntityNode entity, string destination)
        {
            List<string> errors = new();
            string directory = "../../out/" + destination + "/OpenApi/Server";
            Directory.CreateDirectory(directory);
            using (StreamWriter writer = new StreamWriter(File.Create("..//..//out//" + destination + "//OpenAPI//Server//specServer.yaml")))
            {
                //Write front-matter
                writer.Write(
                    "openapi: 3.0.0\n" +
                    "info:\n" +
                    "  description: >-\n" +
                    "    This is an auto-generated server-API-component for the " + entity.ID + " entity.\n" +
                    "  version: 1.0.0\n" +
                    "  title: " + entity.ID + "\n");
                writer.Flush();
                var dtos = entity.DTOs.Concat(entity.Scope.PropagatedSymbols.Select(x => x.Symbol.ASTNode as DTONode)).ToList();
                if (dtos.Count > 0)
                {
                    //Write tags
                    writer.Write(
                        "tags:\n");
                    foreach (DTONode dto in dtos)
                    {
                        writer.Write(GetTagFromDTO(dto));
                    }
                    writer.Flush();
                }
                //Write calls/paths
                writer.Write("paths:\n");
                Indent();
                foreach (EventDeclNode ev in entity.Controller.EventDeclarations.Where(x => x.EventType == EventTypes.Handler))
                {
                    writer.Write(GetPathsForServer(ev));
                }
                writer.Flush();
                //Write components
                if (dtos.Count > 0)
                {
                    string components = "components:\n";
                    Indent();
                    components += GetTabDepth() + "schemas:\n";
                    Indent();
                    writer.Write(components);
                    foreach (DTONode dto in dtos)
                    {
                        writer.Write(GetSchemaForDTO(dto));
                    }
                    writer.Flush();
                    unindent();
                    unindent();
                }
            }
            Process validator = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = @"java",
                    Arguments = "-jar " + OpenAPIPath + "-jar " + OpenAPIPath + " validate -i ../../out/" + destination.Replace("//", "/") + "/OpenApi/Client/specClient.yaml",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            validator.Start();
            while (!validator.StandardOutput.EndOfStream)
            {
                errors.Add(validator.StandardOutput.ReadLine());
            }
            validator.WaitForExit();
            if (errors.Count == 2 && errors[1] == "No validation issues detected.")
                errors.RemoveRange(0, 2);
            if (errors.Any())
            {
                return errors.Skip(1).ToList();
            }
            Process proc = Process.Start(@"java", "-jar " + OpenAPIPath + " generate -i ../../out/" + destination.Replace("//", "/") + "/OpenAPI/Server/specServer.yaml -g cpp-restbed-server -o ../../out/" + destination.Replace("//", "/") + "/OpenAPI/Server");
            proc.WaitForExit();
            proc.Close();
            proc.Dispose();
            return errors;
        }

        private string GetPathsForServer(EventDeclNode ev)
        {
            string res = GetTabDepth() + "/" + ev.Name + ":\n";
            Indent();
            res += GetTabDepth() + "post:\n";
            Indent();
            res += GetTabDepth() +"summary: Auto-generated endpoint corresponding to the " + ev.Name + " handler\n"
                + GetTabDepth() + "operationId: " + ev.Name + "\n";
            if (ev.InputTypes.Count > 0) 
            {
                res += GetTabDepth() + "parameters:\n";
                Indent();
                int i = 0; //TODO replace with actual object name from machines
                foreach(TypeNode type in ev.InputTypes)
                {
                    res += GetTabDepth() + "- name: " + type.ToString() + i + "\n";
                    Indent();
                    res += GetTabDepth() + "in: query\n"
                        + GetTabDepth() + "description: Auto-generated parameter nr. " + i + "\n"
                        + GetTabDepth() + "schema:";
                    Indent();
                    res += GetTabDepth() + GetAPIType(type);
                    unindent();
                    unindent();
                    i++;
                }
                unindent();
            }
            res += GetTabDepth() + "responses:\n";
            Indent();
            res += ServerAcceptResponse(ev);
            res += ServerErrorResponse() + "\n";
            unindent();
            unindent();
            unindent();
            return res;

        }

        private string GetPathsForClient(KeyValuePair<string, Tuple<IReadOnlyList<Symbol>, IReadOnlyList<Symbol>>> x)
        {
            string res = GetTabDepth() + "/" + x.Key + ":\n";
            Indent();
            res += GetTabDepth() + "post:\n";
            Indent();
            res += GetTabDepth() + "summary: Auto-generated endpoint corresponding to the " + x.Key + " handler\n"
                + GetTabDepth() + "operationId: " + x.Key + "\n";
            if (x.Value.Item1.Count > 0)
            {
                res += GetTabDepth() + "parameters:\n";
                Indent();
                int i = 0; //TODO replace with actual object name from machines
                foreach (Symbol input in x.Value.Item1)
                {
                    res += GetTabDepth() + "- name: " + input.GetFullyQualifiedName().Split(".").Last() + i + "\n";
                    Indent();
                    res += GetTabDepth() + "in: query\n"
                        + GetTabDepth() + "description: Auto-generated parameter nr. " + i + "\n"
                        + GetTabDepth() + "schema:";
                    Indent();
                    TypeNode typ = input.Attributes["Type"] as TypeNode;
                    if (typ == null)
                        throw new Exception("typ error");
                    res += GetTabDepth() + GetAPIType(typ);
                    unindent();
                    unindent();
                    i++;
                }
                unindent();
            }
            res += GetTabDepth() + "responses:\n";
            Indent();
            res += ClientAcceptResponse(x);
            res += ClientErrorResponse(x);
            unindent();
            unindent();
            unindent();
            return res + "\n";

        }

        private string ServerErrorResponse()
        {
            string res = GetTabDepth() + "'400':\n";
            Indent();
            res += GetTabDepth() + "description: Invalid input";
            unindent();
            return res;
        }

        private string ServerAcceptResponse(EventDeclNode ev)
        {
            string res = GetTabDepth() + "'200':\n";
            Indent();
            res += GetTabDepth() + "description: Successful operation\n";
            if(ev.InputTypes.Count == 1) 
            {
                res += GetTabDepth() + "content:\n";
                Indent();
                res += GetTabDepth() + "application/json:\n";
                Indent();
                res += GetTabDepth() + "schema:\n";
                Indent();
                res += GetTabDepth() + GetAPIType(ev.InputTypes[0]);
                unindent();
                unindent();
                unindent();
            }
            else if(ev.InputTypes.Count > 1)
            {
                res += GetTabDepth() + "content:\n";
                Indent();
                res += GetTabDepth() + "application/json:\n";
                Indent();
                res += GetTabDepth() + "schema:\n";
                Indent();
                res += GetTabDepth() + "type: object\n" + GetTabDepth() + "properties:";
                Indent();
                int i = 0;
                foreach(TypeNode type in ev.InputTypes)
                {
                    res += GetTabDepth() + type.ToString() + i + ":\n";
                    Indent();
                    res += GetTabDepth() + GetAPIType(type) + "\n"
                        + GetTabDepth() + "description: Auto-generated property nr. " + i + "\n";
                    unindent();
                    ++i;
                }
                unindent();
                unindent();
                unindent();
                unindent();
            }
            unindent();
            return res;
        }

        private string ClientErrorResponse(KeyValuePair<string, Tuple<IReadOnlyList<Symbol>, IReadOnlyList<Symbol>>> x)
        {
            string res = GetTabDepth() + "'400':\n";
            Indent();
            res += GetTabDepth() + "description: Invalid input";
            unindent();
            return res;
        }

        private string ClientAcceptResponse(KeyValuePair<string, Tuple<IReadOnlyList<Symbol>, IReadOnlyList<Symbol>>> x)
        {
            string res = GetTabDepth() + "'200':\n";
            Indent();
            res += GetTabDepth() + "description: Successful operation\n";
            if(x.Value.Item2.Count == 1)
            {
                res += GetTabDepth() + "content:\n";
                Indent();
                res += GetTabDepth() + "application/json:\n";
                Indent();
                res += GetTabDepth() + "schema:\n";
                if(x.Value.Item2[0].SymbolType == SymbolTypes.DTO)
                {
                    Indent();
                    res += GetTabDepth() + "$ref: '#components/schemas/" + x.Key + "'\n:";
                    unindent();
                }
                else 
                {
                    Indent();
                    res += GetTabDepth() + GetAPIType(x.Value.Item2[0].Attributes["Type"] as TypeNode);
                    unindent();
                }
                unindent();
                unindent();
            }else if (x.Value.Item2.Count > 1)
            {
                res += GetTabDepth() + "content:\n";
                Indent();
                res += GetTabDepth() + "application/json:\n";
                Indent();
                res += GetTabDepth() + "schema:\n";
                Indent();
                res += GetTabDepth() + "type: array\n" + GetTabDepth() + "items:\n";
                Indent();
                res += GetTabDepth() + "anyOf:\n";
                Indent();
                foreach(Symbol s in x.Value.Item2)
                {
                    if(s.ASTNode is DTONode node)
                    {
                        res += GetTabDepth() + "- $ref: '#/components/schemas/" + node.ID + "'\n";
                    }
                    else
                    {
                        res += GetTabDepth() + GetAPIType(s.ASTNode as TypeNode);
                    }

                }
                unindent();
                unindent();
                unindent();
                unindent();
                unindent();
            }
            unindent();
            return res;
        }

        private string GetTagFromDTO(DTONode dto)
        {
            return "  - name: " + dto.ID + "\n    description: auto-generated DTO " + dto.ID + ".\n";
        }

        private string GetSchemaForDTO(DTONode dto)
        {
            Indent();
            string schema = "";
            schema += GetTabDepth() + dto.ID + ":\n";
            Indent();
            if (dto.Base != null)
            {
                schema += GetTabDepth() + "allof:\n";
                Indent();
                schema += GetTabDepth() + "- $ref: '#components/schemas/" + dto.Base + "'\n" +
                    "- type: object\n";

            }
            schema +=
                GetTabDepth() + "title: a " + dto.ID + "\n" +
                GetTabDepth() + "description: An auto-generated class for the " + dto.ID + " dto.\n" +
                GetTabDepth() + "type: object\n" +
                GetTabDepth() + "properties:\n";
            Indent();
            foreach (Field f in dto.Fields) 
            {
                schema += GetTabDepth() + f.Name + ":";
                Indent();
                schema += GetTabDepth() + GetAPIType(f.Type);
                unindent();
            }
            unindent();
            unindent();
            unindent();
            if (dto.Base != null)
                unindent();
            return schema;
        }

        private string GetAPIType(TypeNode type)
        {
            string res = "\n" + GetTabDepth();
            if (type is CollectionTypeNode collec)
            {
                if (collec.CollectionType == CollectionTypes.List)
                {
                    res += "type: array\n" + GetTabDepth() + "items:\n";
                    Indent();
                    res += GetTabDepth() + GetAPIType(collec.ElementType) +  "\n";
                    unindent();
                    return res;
                }
                else if (collec.CollectionType == CollectionTypes.Set) 
                {
                    res +=  "type: array\n" + GetTabDepth() + "items:\n";
                    Indent();
                    res += GetTabDepth() + GetAPIType(collec.ElementType) + "\n" + GetTabDepth() + "uniqueItems: true\n";
                    unindent();
                    return res;
                }
                else
                    throw new Exception("Unknown collection type " + collec.CollectionType + " encountered during API gen");
            }
            else if (type is PrimitiveTypeNode prim)
            {
                switch (prim.Type)
                {
                    case PrimitiveTypes.Bool:
                        res += "type: boolean\n";
                        return res;
                    case PrimitiveTypes.Byte:
                        res += "type: string\n" + GetTabDepth() + "format: byte\n"; //or binary, if desired
                        return res;
                    case PrimitiveTypes.Char:
                        {
                         res += "type: string\n";
                            unindent();
                            res += GetTabDepth() + "minLength: 1\n" + GetTabDepth() + "maxLength: 1\n";
                            Indent();
                            return res;
                        }
                    case PrimitiveTypes.Int:
                        res += "type: integer\n";
                        return res;
                    case PrimitiveTypes.Real:
                        res += "type: double\n";
                        return res;
                    case PrimitiveTypes.String:
                        res += "type: string\n";
                        return res;
                    case PrimitiveTypes.ByteSequence:
                        {
                            res += "type: array\nitems:\n" + GetTabDepth() + "type: string\n";
                            Indent();
                            res += GetTabDepth() +"format: byte\n";
                            unindent();
                            return res;
                        }
                    case PrimitiveTypes.Void:
                        {
                            res += "type: string\n";
                            unindent();
                            res += GetTabDepth() + "nullable: true\n";
                            Indent();
                            return res;
                        }
                    default:
                        throw new Exception("Encountered unknown primitive type " + prim.Type + " during API gen");
                }
            }
            else if (type is TupleTypeNode tup)
            {
                res += "type: array\n" + GetTabDepth() + "items:\n";
                Indent();
                res += GetTabDepth() + "oneOf:\n";
                Indent();
                foreach (TypeNode x in tup.ElementTypes)
                {
                    res += GetTabDepth() + "- " + GetAPIType(x) + "\n";
                }
                unindent();
                unindent();
                return res;
            }
            else if (type is UserDefTypeNode usrdef)
            {
                res += "$ref: '#/components/schemas/" + usrdef.UserTypeID.Split(".").Last() + "'\n";
                return res;
            }
            else
                throw new Exception("Unknown type encountered at API generation.");
        }

        private int tagDepth = 0;

        private void Indent()
        {
            tagDepth += 1;
        }
        private void unindent()
        {
            if (tagDepth == 0)
                throw new Exception("Cannot unindent below 0 tabs");
            tagDepth -= 1;
        }
        private string GetTabDepth()
        {
            string tabs = "";
            for(int i = 0; i < tagDepth; ++i)
            {
                tabs += "  ";
            }
            return tabs;
        }
    }
}
