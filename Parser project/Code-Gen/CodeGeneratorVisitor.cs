﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using XMLTester.AST;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.AST.Structural;
using XMLTester.AST.Structural.SubSystems;
using XMLTester.SemanticAnalysis;

namespace XMLTester.Code_Gen
{
    class CodeGeneratorVisitor : IASTVisitor<List<string>>
    {
        APIGenerator apigen;
        ICGenerator icgen;

        public CodeGeneratorVisitor(string openAPIPath)
        {
            apigen = new(openAPIPath);
            icgen = new();
            if(Directory.Exists("..//..//out//"))
                Directory.Delete("..//..//out//", true);
        }

        public List<string> Visit(ModelNode node)
        {
            List<string> errors = new();
            foreach(var dependency in node.Dependencies)
            {
                errors.AddRange(dependency.Accept(this));
            }
            errors.AddRange(node.MainSoS.Accept(this));
            return errors;
        }

        public List<string> Visit(SoSNode node)
        {
            List<string> errors = new();
            foreach(var component in node.SubSystems)
            {
                errors.AddRange(component.Accept(this));
            }
            return errors;
        }

        public List<string> Visit(LinkNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(ConstituentNode node)
        {
            List<string> errors = new();
            foreach (var component in node.SubSystems)
            {
                errors.AddRange(component.Accept(this));
                    
            }
            return errors;
        }

        public List<string> Visit(EntityNode node)
        {
            List<string> errors = new();
            string destination = Regex.Replace(node.Scope.GetFullyQualifiedName(), @"\.", @"//");
            errors.AddRange(apigen.Generate(node, destination));
            foreach (var IC in node.InternalClasses)
            {
                icgen.generate(IC, destination);
            }
            return errors;
        }

        public List<string> Visit(ReferenceNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(ControllerNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(EventDeclNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(ActionGraphTreeRootNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(ActionTransitionNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(CommunicatorNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(MachineNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(InvocationGraphTreeRootNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(InvocationTransitionNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(DTONode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(CLSNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(PrimitiveTypeNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(CollectionTypeNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(TupleTypeNode node)
        {
            throw new NotImplementedException();
        }

        public List<string> Visit(UserDefTypeNode node)
        {
            throw new NotImplementedException();
        }
    }
}
