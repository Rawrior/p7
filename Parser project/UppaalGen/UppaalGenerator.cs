using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using XMLTester.AST;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.AST.Structural;
using XMLTester.AST.Structural.SubSystems;
using XMLTester.Exceptions;
using XMLTester.SemanticAnalysis;
using XMLTester.SymbolTables;
using XMLTester.UppaalGen.Builder;

namespace XMLTester.UppaalGen
{
    public class UppaalGenerator : IASTVisitor<object>
    {
        private const int NODE_DISTANCE = 102;

        private Dictionary<EntityNode, Dictionary<string, HashSet<string>>> _clockUsage;
        private Dictionary<EntityNode, int> _entityToTemplateNumberMap;
        private Units _minimumUnit;

        // Used to ensure that calls to a reusable part of a UPPAAL machine "returns" to the correct location.
        private int _callCounter;


        public UppaalModel ComputeUppaalModel(ModelNode node)
        {
            // Templates
            ClockUsageAnalysis clockCountAnalysis = new ClockUsageAnalysis();
            _clockUsage = clockCountAnalysis.Visit(node);
            _entityToTemplateNumberMap = _clockUsage.Keys.Select((ent, index) => (ent, index)).ToDictionary(key => key.ent, value => value.index);
            _minimumUnit = GetMinimalUsedUnit(_clockUsage.Keys);

            List<Template> uppaalTemplates = (node.Accept(this) as IEnumerable<Template>).ToList();

            // Global declarations
            int numberOfEntities = uppaalTemplates.Count;
            int maxNumberOfInstances = uppaalTemplates.Max(tmpl => (int)tmpl.InstanceCount);

            List<string> globals = new List<string>()
            {
                $"const int num_ent = {numberOfEntities};\n" +
                    $"const int max_inst = {maxNumberOfInstances};\n" +
                    "const int max_id = num_ent * max_inst - 1;",
                $"typedef int[0, max_id] id_all;",
                GenerateCountRanges(maxNumberOfInstances)
            };
            globals.AddRange(GenerateChannelDefinitions(uppaalTemplates));

            // System declaration
            string system = $"system {string.Join(',', uppaalTemplates.Select(tmpl => tmpl.Name))};";

            // Queries
            var queries = GenerateQueries(uppaalTemplates);

            return new UppaalModel(
                WriteUppaalModel(globals, uppaalTemplates, system, queries),
                WriteQueries(queries)
            );
        }

        private Units GetMinimalUsedUnit(IEnumerable<EntityNode> entities)
        {
            Units minUnit = Units.Day;
            foreach (EntityNode entity in entities)
                foreach (ActionTransitionNode transition in GetAllActions(entity))
                {
                    // "Max" is used here since the smaller units have higher numeric values.
                    Units localMinUnit = transition.Durations.Max(dur => dur.Item2);
                    if (localMinUnit > minUnit)
                        minUnit = localMinUnit;
                }

            return minUnit >= Units.Second ? minUnit : Units.Second; // It can only go up to seconds
        }

        private IEnumerable<ActionTransitionNode> GetAllActions(EntityNode entity)
        {
            foreach (ActionGraphTreeRootNode root in entity.Controller.GraphTreeRoots)
                foreach (var pair in GetAllActions(root.ActionBranches))
                    yield return pair;
        }

        private IEnumerable<ActionTransitionNode> GetAllActions(IEnumerable<ActionTransitionNode> transitions)
        {
            foreach (ActionTransitionNode transition in transitions)
                foreach (var transition2 in GetAllActions(transition.ActionBranches).Prepend(transition))
                    yield return transition2;
        }

        private static string GenerateCountRanges(int maxNumberOfInstances)
        {
            return string.Join('\n', Enumerable.Range(1, maxNumberOfInstances).Select(cnt => $"typedef int[0,{cnt-1}] cnt_{cnt};"));
        }

        private static List<string> GenerateChannelDefinitions(List<Template> templates)
        {
            List<string> definitions = new List<string>();

            foreach (Template template in templates)
            {
                StringBuilder sb = new StringBuilder($"// Channels originating from '{template.Name}'.");
                foreach (string channel in template.OwnedChannels)
                    sb.Append($"\nchan bgn_{channel}[id_all], end_{channel}[id_all];");
                definitions.Add(sb.ToString());
            }

            return definitions;
        }

        private List<Query> GenerateQueries(List<Template> uppaalTemplates)
        {
            List<Query> queries = new List<Query>();

            foreach (Template template in uppaalTemplates)
            {
                // Find all templates to which "template" initiate communication, or which initiates communication with "template".
                List<Template> interactedWithTemplates =
                    uppaalTemplates.Where(tmpl => template.SyncronizedEntityIDs.Contains(tmpl.Name) || tmpl.SyncronizedEntityIDs.Contains(template.Name)).ToList();

                // Find all transitions that enter a state with multiple ingoing transitions.
                IEnumerable<Transition> mergerTransitions
                    = template.Transitions.GroupBy(tran => tran.TargetID).Where(grp => grp.Count() >= 2).SelectMany(grp => grp);

                foreach (Location location in template.Locations)
                {
                    queries.Add(new Query($"E<> exists (i:cnt_{template.InstanceCount}) {template.Name}(i).{location.Name}", null));
                }

                // We only generate queries for mergerTransitions, since, getting to the source location of the merger transition
                // implies that it is possible to take all transitions since the previous merger location.
                foreach (Transition transition in mergerTransitions)
                {
                    Location sourceLocation = template.Locations.First(loc => loc.ID == transition.SourceID);
                    if (sourceLocation.Urgent)
                        continue; // No need to make query, since these can always be taken.

                    List<string> transitionConditions = new();

                    string guard = transition.Guard?
                        .Replace("clk_", $"{template.Name}(i).clk_")
                        .Replace("GetReturn", $"{template.Name}(i).GetReturn");

                    bool injectGuardIntosyncGuards = guard == "other == id";
                    if (injectGuardIntosyncGuards)
                        guard = null;

                    if (guard != null)
                        transitionConditions.Add(guard);

                    string syncronization = transition.Synchronization;
                    if (syncronization != null)
                    {
                        bool endsWithExclamation = syncronization.EndsWith('!');
                        bool startsWithBgn = syncronization.StartsWith("bgn_");

                        // Find all the transitions from all linked transitions that syncronize on the event/channel of "transition".
                        // If "transition" has channel "chan!", find all transitions with channel "chan?", and vice versa
                        List<(Template, Transition)> syncTransitions =
                            interactedWithTemplates.SelectMany(tmpl =>
                                tmpl.Transitions.Where(tran =>
                                    tran.BaseChannelName == transition.BaseChannelName &&
                                    tran.Synchronization.StartsWith("bgn_") == startsWithBgn &&
                                    tran.Synchronization.EndsWith('!') != endsWithExclamation &&
                                    tran != transition
                                ).Select(tran => (tmpl, tran))
                            ).ToList();

                        // Generate query to ensure that at least one of the syncTransitions are "takeable" at the time of taking "transition".
                        // To do this we just need to ensure that the guard of the syncTransition is satisfied.
                        List<string> syncConditions = new();
                        foreach ((Template syncTemplate, Transition syncTransition) in syncTransitions)
                        {
                            Location syncSourceLocation = syncTemplate.Locations.First(loc => loc.ID == syncTransition.SourceID);
                            string syncGuard =
                                syncTransition.Guard?
                                .Replace("clk_", $"{syncTemplate.Name}(x).clk_")
                                .Replace("GetReturn", $"{syncTemplate.Name}(x).GetReturn")
                                .Replace("other == id", $"{template.Name}(i).mostRecentCaller == {syncTemplate.Name}(x).id");

                            if (injectGuardIntosyncGuards)
                            {
                                string inject = $"{syncTemplate.Name}(x).mostRecentCaller == {template.Name}(i).id";
                                syncGuard = string.IsNullOrEmpty(syncGuard) ? inject : syncGuard + " && " + inject;
                            }

                            if (string.IsNullOrEmpty(syncGuard))
                                syncConditions.Add($"(exists (x:cnt_{syncTemplate.InstanceCount}) {syncTemplate.Name}(x).{syncSourceLocation.Name})");
                            else
                                syncConditions.Add(
                                    $"(exists (x:cnt_{syncTemplate.InstanceCount}) ({syncTemplate.Name}(x).{syncSourceLocation.Name} && {syncGuard}))"
                                );
                        }

                        transitionConditions.Add(
                            $"({string.Join(" || ", syncConditions)})"
                        );
                    }

                    queries.Add(new Query($"E<> exists (i:cnt_{template.InstanceCount}) ({template.Name}(i).{sourceLocation.Name} && ({string.Join(" && ", transitionConditions)}))", null));
                }

                queries.Add(new Query());
            }

            return queries;
        }


        private string WriteUppaalModel(List<string> globals, List<Template> uppaalTemplates, string system, List<Query> queries)
        {
            string fullGlobals = string.Join("\n\n", globals);

            StringBuilder sbResult = new();
            var settings = new XmlWriterSettings() { Encoding = Encoding.UTF8, OmitXmlDeclaration = true, Indent = true, IndentChars = "\t", NewLineChars = "\n" };
            using (XmlWriter xmlWriter = XmlWriter.Create(sbResult, settings))
            {
                xmlWriter.WriteRaw("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
                xmlWriter.WriteRaw("<!DOCTYPE nta PUBLIC '-//Uppaal Team//DTD Flat System 1.1//EN' 'http://www.it.uu.se/research/group/darts/uppaal/flat-1_2.dtd'>\n");
                xmlWriter.WriteStartElement("nta");

                xmlWriter.WriteStartElement("declaration");
                xmlWriter.WriteString(fullGlobals);
                xmlWriter.WriteEndElement();

                foreach (Template template in uppaalTemplates)
                {
                    Dictionary<uint, int> locationToYCoordinate = new();
                    int coordinateY = 0;

                    xmlWriter.WriteStartElement("template");

                    xmlWriter.WriteElementString("name", template.Name);
                    xmlWriter.WriteElementString("parameter", template.Parameter);
                    xmlWriter.WriteElementString("declaration", string.Join("\n\n", template.Declarations));

                    foreach (Location location in template.Locations)
                    {
                        xmlWriter.WriteStartElement("location");
                        xmlWriter.WriteAttributeString("id", $"id{location.ID}");
                        xmlWriter.WriteAttributeString("x", $"0"); // Make positioning later
                        xmlWriter.WriteAttributeString("y", $"{coordinateY}"); // Make positioning later
                        {
                            xmlWriter.WriteStartElement("name");
                            xmlWriter.WriteAttributeString("x", $"15"); // Make positioning later
                            xmlWriter.WriteAttributeString("y", $"{coordinateY - 15}"); // Make positioning later
                            xmlWriter.WriteString(location.Name.Replace('.', '_'));
                            xmlWriter.WriteEndElement();
                        }
                        string invariant = location.Invariant;
                        if (!string.IsNullOrEmpty(invariant))
                        {
                            xmlWriter.WriteStartElement("label");
                            xmlWriter.WriteAttributeString("kind", "invariant");
                            xmlWriter.WriteAttributeString("x", $"15"); // Make positioning later
                            xmlWriter.WriteAttributeString("y", $"{coordinateY}"); // Make positioning later
                            xmlWriter.WriteString(invariant);
                            xmlWriter.WriteEndElement();
                        }
                        if (location.Urgent)
                            xmlWriter.WriteElementString("urgent", null);
                        xmlWriter.WriteEndElement(); // location

                        locationToYCoordinate[location.ID] = coordinateY;
                        coordinateY += NODE_DISTANCE;
                    }

                    xmlWriter.WriteStartElement("init");
                    xmlWriter.WriteAttributeString("ref", $"id{template.Init.ID}");
                    xmlWriter.WriteEndElement();

                    foreach (Transition transition in template.Transitions)
                    {
                        xmlWriter.WriteStartElement("transition");

                        xmlWriter.WriteStartElement("source");
                        xmlWriter.WriteAttributeString("ref", $"id{transition.SourceID}");
                        xmlWriter.WriteEndElement();

                        xmlWriter.WriteStartElement("target");
                        xmlWriter.WriteAttributeString("ref", $"id{transition.TargetID}");
                        xmlWriter.WriteEndElement();

                        if (transition.Select != null)
                            PrintLabel(xmlWriter, "select", 15, locationToYCoordinate[transition.SourceID] + 15, transition.Select);

                        if (transition.Guard != null)
                            PrintLabel(xmlWriter, "guard", 15, locationToYCoordinate[transition.SourceID] + 15*3, transition.Guard);

                        if (transition.Synchronization != null)
                            PrintLabel(xmlWriter, "synchronisation", 15, locationToYCoordinate[transition.SourceID] + 15*2, transition.Synchronization);

                        if (transition.Assignment != null)
                            PrintLabel(xmlWriter, "assignment", 15, locationToYCoordinate[transition.SourceID] + 15*4, transition.Assignment);

                        xmlWriter.WriteEndElement(); // transition
                    }

                    xmlWriter.WriteEndElement(); // template
                }

                xmlWriter.WriteElementString("system", system);

                xmlWriter.WriteStartElement("queries");
                foreach (Query query in queries)
                {
                    xmlWriter.WriteStartElement("query");
                    xmlWriter.WriteElementString("formula", query.Formula);
                    xmlWriter.WriteElementString("comment", query.Comment);
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement(); // queries

                xmlWriter.WriteEndElement(); // nts
                xmlWriter.Close();
            }

            return sbResult.ToString()
                .Replace("<formula />", "<formula></formula>")
                .Replace("<comment />", "<comment></comment>"); // We have to do this else the verifyer wont accept it through the commandline
        }

        private static void PrintLabel(XmlWriter xmlWriter, string kind, int x, int y, string content)
        {
            xmlWriter.WriteStartElement("label");
            xmlWriter.WriteAttributeString("kind", kind);
            xmlWriter.WriteAttributeString("x", $"{x}");
            xmlWriter.WriteAttributeString("y", $"{y}");
            xmlWriter.WriteString(content.Replace('.', '_'));
            xmlWriter.WriteEndElement();
        }


        private string WriteQueries(List<Query> queries)
        {
            return string.Join("\n\n",
                queries.Select(q => $"/*\n{q.Comment ?? ""}\n*/\n{q.Formula ?? "//NO_QUERY"}").Prepend("//This file was generated from the first ACE compiler")
            );
        }


        #region Used visitors

        public object Visit(ModelNode node)
        {
            IEnumerable<Template> result = node.Dependencies
                .SelectMany(dep => (IEnumerable<Template>)dep.Accept(this))
                .Concat((IEnumerable<Template>)node.MainSoS.Accept(this));

            return result;
        }

        public object Visit(SoSNode node)
        {
            return node.SubSystems
                .SelectMany(sub => (IEnumerable<Template>)sub.Accept(this));
        }

        public object Visit(ConstituentNode node)
        {
            IEnumerable<Template> result = node.SubSystems
                .SelectMany(sub => (IEnumerable<Template>)sub.Accept(this));

            return result;
        }

        public object Visit(EntityNode node)
        {
            string fullyQualifiedName = node.Scope.GetFullyQualifiedName();
            uint numUppaalInstances =
                GetBoundedMultiplicitiesForEntity(node.Scope.Parent, fullyQualifiedName)
                .Concat(GetBoundedMultiplicitiesForEntity(node.Scope.Parent.Parent, fullyQualifiedName))
                .Append(1u) // The minimum allowed number of instances
                .Max();

            if ((uint)node.Count > 0)
            {
                if (numUppaalInstances > (uint)node.Count)
                    throw new ArgumentException($"The entity '{fullyQualifiedName}' requires more connections than that of its count", nameof(node));
                else
                    numUppaalInstances = Math.Max((uint)node.Count, numUppaalInstances);
            }

            int maxAllocatedClocks = _clockUsage[node].Values.Max(activeClocks => activeClocks.Count);
            UppaalMachine machine = GenerateUppaalMachine(node, _clockUsage[node], maxAllocatedClocks);

            // Local declarations
            List<string> declarations = new()
            {
                $"int id = {_entityToTemplateNumberMap[node]}*max_inst+instance_id;\n" +
                    $"int mostRecentCaller = -1;",
                $"clock {GenerateClockNames(maxAllocatedClocks)};",
                GetCallStackDeclarations(node.Communicators.Max(mac => mac.Machines.Count)) // Call-depth cannot surpass this, since we avoid recursive calls.
            };

            // A list is returned to make handling of returned values easier.
            return new List<Template>() {
                new Template(
                    node.Scope.GetFullyQualifiedName().Replace('.', '_'),
                    numUppaalInstances,
                    declarations,
                    machine.Locations,
                    machine.Init,
                    machine.Transitions,
                    node.Scope,
                    machine.SyncronizedEntityIDs.ToList(),
                    machine.OwnedChannels.ToList())
            };
        }

        #endregion

        #region Helpers

        private IEnumerable<uint> GetBoundedMultiplicitiesForEntity(SymbolTable scope, string fullyQuallifiedEntityID)
        {
            if (scope == null)
                return new List<uint>();

            return scope.Symbols
                .Where(sym => sym.Value.SymbolType == SymbolTypes.Link)
                .Select(sym => (LinkNode)sym.Value.ASTNode).ToList()
                .SelectMany(
                    link => link.Connections.Where(con => con.EntityID == fullyQuallifiedEntityID && con.Multiplicity != 0).Select(con => con.Multiplicity + (link.Connections.Count(con2 => con2.EntityID == fullyQuallifiedEntityID) == 1 ? 0u : 1u))
                );
        }


        private string GenerateClockNames(int maxClockCount)
        {
            return string.Join(',', Enumerable.Range(0, maxClockCount).Select(num => $"clk_{num}"));
        }

        private string GetCallStackDeclarations(int maxCallDepth)
        {
            return
                $"int callStackNames[{maxCallDepth}];\n" +
                $"int callStackReturns[{maxCallDepth}];\n" +
                "int currentLevel = -1;\n" +
                "void PushStack(int eventName, int _returnTo) {\n" +
                "    currentLevel++;\n" +
                "    callStackNames[currentLevel] = eventName;\n" +
                "    callStackReturns[currentLevel] = _returnTo;\n" +
                "}\n" +
                "void PopStack() {\n" +
                "    callStackNames[currentLevel] = 0;\n" +
                "    callStackReturns[currentLevel] = 0;\n" +
                "    currentLevel--;\n" +
                "}\n" +
                "int GetReturn() {\n" +
                "    return callStackReturns[currentLevel];\n" +
                "}\n" +
                "bool StackContainsCall(int eventName) {\n" +
                "    int i = 0;\n" +
                "    while (i <= currentLevel)\n" +
                "        if (callStackReturns[i] == eventName)\n" +
                "            return true;\n" +
                "        else\n" +
                "            i++;\n" +
                "    return false;\n" +
                "}";
        }


        private UppaalMachine GenerateUppaalMachine(EntityNode entity, Dictionary<string, HashSet<string>> clockUsage, int maxAllocatedClocks)
        {
            UppaalMachine machine = new UppaalMachine(
                entity.Controller.GraphTreeRoots[0].RootState, _entityToTemplateNumberMap[entity], GenerateMachineIDs(entity)
            );

            _callCounter = 0;

            ActionGraphTreeRootNode startRoot = entity.Controller.GraphTreeRoots[0];
            Dictionary<string, Dictionary<string, uint>> clockAllocation = new() {
                {
                    startRoot.RootState,
                    clockUsage[startRoot.RootState]
                        .Select((state, index) => (state, index))
                        .ToDictionary(key => key.state, value => (uint)value.index)
                }
            };

            foreach (ActionGraphTreeRootNode root in entity.Controller.GraphTreeRoots)
                GenerateUppaalControllerMachine(entity, machine, root.RootState, root.ActionBranches, clockUsage, clockAllocation, maxAllocatedClocks);

            return machine;
        }

        // The clockAllocation parameter is a map as such: StateName -> (TimedState -> ClockID)
        private void GenerateUppaalControllerMachine(
            EntityNode entity,
            UppaalMachine uppaalMachine,
            string startState,
            IReadOnlyList<ActionTransitionNode> transitions,
            Dictionary<string, HashSet<string>> clockUsage,
            Dictionary<string, Dictionary<string, uint>> clockAllocation,
            int maxAllocatedClocks)
        {
            HashSet<string> startStateClockUsage = clockUsage[startState];
            Dictionary<string, uint> startStateClockAllocation = clockAllocation[startState];

            HashSet<uint> clocksToReset = ReallocateClocks(startStateClockUsage, startStateClockAllocation, maxAllocatedClocks);
            IEnumerable<uint> unallocatedClocks = Enumerable.Range(0, maxAllocatedClocks).Select(clock => (uint)clock).Except(startStateClockAllocation.Values);
            clocksToReset.UnionWith(unallocatedClocks);

            foreach (ActionTransitionNode transition in transitions)
            {
                Location startLoc = uppaalMachine.GetOrCreateControllerLocation(startState);
                Location endLoc = uppaalMachine.GetOrCreateControllerLocation(transition.EndState);

                clockAllocation.TryAdd(transition.EndState, new Dictionary<string, uint>());
                MoveLocalAllocationsToNextState(startStateClockAllocation, clockAllocation[transition.EndState]);

                uint timeConstraint = GetTimeFromDurations(transition.Durations, _minimumUnit);
                ActionConversionInfo conversionInfo = new ActionConversionInfo(
                    startStateClockAllocation, clocksToReset, timeConstraint, _callCounter++ // Incremented such that this value is different next time
                );

                string actionName = transition.ActionName.Replace(":", ""); // Remove handler markers
                if (actionName == "!")
                {
                    // TODO: State invariants
                    // TODO: We cannot as of now generate error handlers. Future works stuff.
                    throw new NotImplementedException("Error handlers are currently not supported");
                }
                else if (string.IsNullOrEmpty(actionName)) // A delay transition
                {
                    uppaalMachine.CreateActionTransition(
                        startLoc, endLoc, transition, ActionTypes.Delay, conversionInfo
                    );

                    //startLoc.AddInvatiant($"clk_{startStateClockAllocation[transition.TimedStateID]} <= {timeConstraint}");
                }
                else // A handler or initiator
                {
                    CommunicatorNode communicator = entity.Communicators.SingleOrDefault(com => com.EventName == actionName);
                    Location macStart, macEnd;

                    if (communicator != null)
                    {
                        macStart = uppaalMachine.TryGetMachineStartNode(communicator.EventName, communicator.EventName); // First machine has same name as event
                        if (macStart == null)
                        {
                            // Pre-compute the start location of the machine, since this is the only place this can be done unambiguously
                            string communicatorStartStateName = communicator.Machines[0].GraphTreeRoots[0].InvokationBranches[0].EndState;
                            macStart = uppaalMachine.GetOrCreateCommunicatorLocation(communicator.EventName, communicator.Machines[0].MachineName, $"{communicator.Machines[0].MachineName.Split(' ').First()}.{communicatorStartStateName}", createAsNewStartLocation: true);

                            // A communicators link can reside in either the same constituent/SoS as the communicators entity
                            // or it can be in the entities parent's parent, if that entity is a boundary entity.
                            LinkNode communicatorLink = (LinkNode)(
                                entity.Scope.Parent.GetSymbol(communicator.LinkID)?.ASTNode
                                ?? entity.Scope.Parent.Parent?.GetSymbol(communicator.LinkID).ASTNode
                            );

                            // The call to "GenerateUppaalCommunicatorMachine" will further generate "UPPAAL" for the remaining communicator machines
                            foreach (InvocationGraphTreeRootNode root in communicator.Machines[0].GraphTreeRoots)
                                GenerateUppaalCommunicatorMachine(communicator, communicatorLink, communicator.Machines[0], uppaalMachine, entity.Scope, root.RootState, root.InvokationBranches);
                        }
                        macEnd = uppaalMachine.GetOrCreateMachineEndLocation(communicator.EventName, communicator.EventName);
                    }
                    else
                        macStart = macEnd = uppaalMachine.GetOrCreateControllerLocation($"{actionName}_atomic");

                    bool isHandlerEvent = entity.Scope.GetSymbol(actionName).SymbolType == SymbolTypes.Handler;

                    uppaalMachine.CreateActionTransition(
                        startLoc, macStart, transition, isHandlerEvent ? ActionTypes.StartHandler : ActionTypes.StartInitiator, conversionInfo
                    );

                    uppaalMachine.CreateActionTransition(
                        macEnd, endLoc, transition, isHandlerEvent ? ActionTypes.EndHandler : ActionTypes.EndInitiator, conversionInfo
                    );
                }

                GenerateUppaalControllerMachine(
                    entity, uppaalMachine, transition.EndState, transition.ActionBranches, clockUsage, clockAllocation, maxAllocatedClocks
                );
            }
        }

        private void GenerateUppaalCommunicatorMachine(
            CommunicatorNode communicator,
            LinkNode communicatorLink,
            MachineNode communicatorMachine,
            UppaalMachine uppaalMachine,
            SymbolTable currentScope,
            string sourceState,
            IReadOnlyList<InvocationTransitionNode> transitions)
        {
            // Skip the variable declaration transition.
            // If this transition ends in the machine's start transition, we know "startState" is actually the modelled start state, which is excluded in uppaal.
            Location macStart = uppaalMachine.TryGetMachineStartNode(communicator.EventName, communicatorMachine.MachineName);
            if (transitions.Count == 1 && macStart.Name.EndsWith(transitions[0].EndState))
            {
                GenerateUppaalCommunicatorMachine(communicator, communicatorLink, communicatorMachine, uppaalMachine, currentScope, transitions[0].EndState, transitions[0].InvokationBranches);
                return;
            }

            foreach (InvocationTransitionNode transition in transitions)
            {
                string machineName = communicatorMachine.MachineName.Split(' ').First();
                Location sourceLoc = uppaalMachine.GetOrCreateCommunicatorLocation(communicator.EventName, machineName, $"{machineName}.{sourceState}", createAsNewStartLocation: false);

                if (transition.InvokationOperation is InputOutputOperation)
                {
                    Location terminationLoc = uppaalMachine.GetOrCreateMachineEndLocation(communicator.EventName, machineName);
                    uppaalMachine.CreateInvocationTransition(sourceLoc, terminationLoc, null, InvocationTypes.Blank, null, null);
                    sourceLoc.Urgent = true;
                    continue;
                }

                Location targetLoc = uppaalMachine.GetOrCreateCommunicatorLocation(communicator.EventName, machineName, $"{machineName}.{transition.EndState}", createAsNewStartLocation: false);

                if (transition.InvokationOperation is InvokeEventOperation eventOpr)
                {
                    string fullNameOfInvokedEntity = communicatorLink.Connections.First(link => link.EntityID.EndsWith(eventOpr.EntityName)).EntityID;
                    EntityNode syncEntity = (EntityNode)currentScope.GetScope(fullNameOfInvokedEntity).ASTNode;
                    InvocationConversionInfo conversionInfo = new InvocationConversionInfo(
                        _entityToTemplateNumberMap[syncEntity], _callCounter++
                    );

                    Location intermediateLoc =
                        uppaalMachine.GetOrCreateCommunicatorLocation(communicator.EventName, machineName, $"{machineName}.{sourceState}_{eventOpr.HandlerName}", createAsNewStartLocation: false);

                    uppaalMachine.CreateInvocationTransition(
                        sourceLoc, intermediateLoc, transition, InvocationTypes.StartHandler, conversionInfo, syncEntity.Scope
                    );

                    uppaalMachine.CreateInvocationTransition(
                        intermediateLoc, targetLoc, transition, InvocationTypes.EndHandler, conversionInfo, syncEntity.Scope
                    );
                }
                else if (transition.InvokationOperation is InvokeMachineOperation)
                {
                    // TODO: Machine invocations must be for a future semester.
                    throw new NotImplementedException();
                }
                else if (transition.InvokationOperation is InvokeFunctionOperation funcOpr)
                {
                    Location intermediateLoc =
                        uppaalMachine.GetOrCreateCommunicatorLocation(communicator.EventName, machineName, $"{machineName}.{sourceState}_{funcOpr.InvokationName}", createAsNewStartLocation: false);
                    intermediateLoc.Urgent = true;

                    uppaalMachine.CreateInvocationTransition(sourceLoc, intermediateLoc, null, InvocationTypes.Blank, null, null);
                    uppaalMachine.CreateInvocationTransition(intermediateLoc, targetLoc, null, InvocationTypes.Blank, null, null);
                }

                GenerateUppaalCommunicatorMachine(communicator, communicatorLink, communicatorMachine, uppaalMachine, currentScope, transition.EndState, transition.InvokationBranches);
            }
        }


        private Dictionary<string, int> GenerateMachineIDs(EntityNode entity)
        {
            return
                entity.Communicators
                .SelectMany(com => com.Machines.Select(mac => mac.MachineName.Split(' ').First())) // Split.First removes any "[LNK:LinkName]" from the machine name 
                .Select((mac, index) => (mac, index))
                .ToDictionary(key => key.mac, value => value.index);
        }


        private HashSet<uint> ReallocateClocks(HashSet<string> localClockUsage, Dictionary<string, uint> localClockAllocation, int maxAllocatedClocks)
        {
            HashSet<uint> clocksToReset = new HashSet<uint>();

            // Deallocate unused clocks
            foreach (string timedState in localClockAllocation.Keys)
                if (!localClockUsage.Contains(timedState))
                    localClockAllocation.Remove(timedState);

            // Allocate new clock (if this is needed; only one new clock may be allocated)
            foreach (string timedState in localClockUsage)
                if (!localClockAllocation.ContainsKey(timedState))
                {
                    int clockIDToAllocate = Enumerable.Range(0, maxAllocatedClocks).First(clockID => !localClockAllocation.ContainsValue((uint)clockID));
                    localClockAllocation[timedState] = (uint)clockIDToAllocate;
                    clocksToReset.Add((uint)clockIDToAllocate);
                    break;
                }

            return clocksToReset;
        }

        private void MoveLocalAllocationsToNextState(Dictionary<string, uint> localClockAllocation, Dictionary<string, uint> nextClockAllocation)
        {
            foreach ((string timedState, uint clockID) in localClockAllocation)
            {
                if (!nextClockAllocation.TryAdd(timedState, clockID) && nextClockAllocation[timedState] != clockID)
                    throw new ClockAllocationException(timedState, clockID, nextClockAllocation[timedState]);
            }
        }

        private uint GetTimeFromDurations(IReadOnlyList<Tuple<uint, Units>> durations, Units outputUnit)
        {
            uint time = 0u;

            foreach (var duration in durations)
            {
                uint multiplier = outputUnit switch
                {
                    Units.Microsecond => duration.Item2 switch
                    {
                        Units.Microsecond => 1u,
                        Units.Millisecond => 1_000u,
                        Units.Second => 1_000_000u,
                        Units.Minute => 1_000_000u * 60u,
                        Units.Hour => throw new ArgumentException($"UPPAAL cannot support 'Hour' measured in the output unit: {outputUnit}"),
                        Units.Day => throw new ArgumentException($"UPPAAL cannot support 'Day' measured in the output unit: {outputUnit}"),
                        _ => throw new NotImplementedException($"Unknown unit: '{duration.Item2}'"),
                    },
                    Units.Millisecond => duration.Item2 switch
                    {
                        Units.Microsecond => throw new ArgumentException($"Microsecond is too low for output unit: '{outputUnit}'"),
                        Units.Millisecond => 1u,
                        Units.Second => 1_000u,
                        Units.Minute => 1_000u * 60u,
                        Units.Hour => 1_000u * 60u * 60u,
                        Units.Day => 1_000u * 60u * 60u * 24u,
                        _ => throw new NotImplementedException($"Unknown unit: '{duration.Item2}'"),
                    },
                    _ => duration.Item2 switch
                    {
                        Units.Microsecond => throw new ArgumentException($"Microsecond is too low for output unit: '{outputUnit}'"),
                        Units.Millisecond => throw new ArgumentException($"Microsecond is too low for output unit: '{outputUnit}'"),
                        Units.Second => 1u,
                        Units.Minute => 60u,
                        Units.Hour => 60u * 60u,
                        Units.Day => 60u * 60u * 24u,
                        _ => throw new NotImplementedException($"Unknown unit: '{duration.Item2}'"),
                    }
                };

                time += duration.Item1 * multiplier;
            }

            return time;
        }

        #endregion

        #region Unused visitors

        public object Visit(LinkNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(ReferenceNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(ControllerNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(EventDeclNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(ActionGraphTreeRootNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(ActionTransitionNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(CommunicatorNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(MachineNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(InvocationGraphTreeRootNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(InvocationTransitionNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(DTONode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(CLSNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(PrimitiveTypeNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(CollectionTypeNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(TupleTypeNode node)
        {
            throw new NotImplementedException();
        }

        public object Visit(UserDefTypeNode node)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
