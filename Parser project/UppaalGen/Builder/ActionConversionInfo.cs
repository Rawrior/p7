﻿using System;
using System.Collections.Generic;

namespace XMLTester.UppaalGen.Builder
{
    public class ActionConversionInfo
    {
        public Dictionary<string, uint> StateToClockMap { get; }
        public HashSet<uint> ClocksToReset { get; }
        public uint TimeConstraint { get; }
        public int CallNumber { get; }

        public ActionConversionInfo(
            Dictionary<string, uint> stateToClockMap, HashSet<uint> clocksToReset, uint timeConstraint, int callNumber)
        {
            StateToClockMap = stateToClockMap;
            ClocksToReset = clocksToReset;
            TimeConstraint = timeConstraint;
            CallNumber = callNumber;
        }
    }
}
