﻿namespace XMLTester.UppaalGen.Builder
{
    public class InvocationConversionInfo
    {
        public int SyncTemplateID { get; }
        public int CallNumber { get; }

        public InvocationConversionInfo(int syncTemplateID, int callNumber)
        {
            SyncTemplateID = syncTemplateID;
            CallNumber = callNumber;
        }
    }
}