﻿ using System;
using System.Collections.Generic;
using XMLTester.SymbolTables;

namespace XMLTester.UppaalGen
{
    public class Transition
    {
        public uint SourceID { get; }
        public uint TargetID { get; }
        public string Select { get; }
        public string Guard { get; }
        public string Synchronization { get; }
        public string Assignment { get; }

        public string BaseChannelName { get; }
        public SymbolTable SyncTarget { get; }

        public Transition(
            uint sourceID, uint targetID,
            string select, string guard, string synchronization, string assignment,
            string baseChannel, SymbolTable syncTarget)
        {
            SourceID = sourceID;
            TargetID = targetID;
            Select = select;
            Guard = guard;
            Synchronization = synchronization;
            Assignment = assignment;

            BaseChannelName = baseChannel;
            SyncTarget = syncTarget;
        }
    }
}
