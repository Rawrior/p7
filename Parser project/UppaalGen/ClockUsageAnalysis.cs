﻿using System;
using System.Collections.Generic;
using System.Linq;
using XMLTester.AST;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.AST.Structural;
using XMLTester.AST.Structural.SubSystems;
using XMLTester.SemanticAnalysis;

namespace XMLTester.UppaalGen
{
    /// <summary>
    /// The return/generic type is a dictionary of the form: "Entity -> (State or transition -> list of active clocks)"
    /// </summary>
    public class ClockUsageAnalysis : IASTVisitor<Dictionary<EntityNode, Dictionary<string, HashSet<string>>>>
    {
        #region Used visitors

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(ModelNode node)
        {
            return node.Dependencies
                .Select(model => model.Accept(this))
                .SelectMany(dict => dict)
                .Concat(node.MainSoS.Accept(this))
                .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(SoSNode node)
        {
            return node.SubSystems
                .Select(sub => sub.Accept(this))
                .SelectMany(dict => dict)
                .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(ConstituentNode node)
        {
            return node.SubSystems
                .Select(sub => sub.Accept(this))
                .SelectMany(dict => dict)
                .ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(EntityNode node)
        {
            // Find the ranges in which clocks are to be used
            var clockBounds = node.Controller.GraphTreeRoots
                .Select(root => ComputeActiveClockEndBounds(root.RootState, root.ActionBranches))
                .SelectMany(dict => dict)
                .ToDictionary(pair => pair.Key, pair => pair.Value);

            // Fill out clock usage in the usage-ranges
            foreach (var tree in node.Controller.GraphTreeRoots.Reverse())
                FillActiveClockBounds(clockBounds, tree.RootState, tree.ActionBranches);

            return new() {
                { node, clockBounds }
            };
        }


        private static Dictionary<string, HashSet<string>> ComputeActiveClockEndBounds(string startState, IReadOnlyList<ActionTransitionNode> transitions)
        {
            int transitionNumber = 0;
            return transitions
                .Select(transition => ComputeActiveClockEndBounds(transition.EndState, transition.ActionBranches))
                .SelectMany(dict => dict)
                .Concat(transitions.Select(
                    transition => new KeyValuePair<string, HashSet<string>>($"{startState}.{transition.EndState}({++transitionNumber})", new HashSet<string>() { transition.TimedStateID }))
                ).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        private static void FillActiveClockBounds(Dictionary<string, HashSet<string>> clockBounds, string startState, IReadOnlyList<ActionTransitionNode> transitions)
        {
            HashSet<string> startStateActiveClocks = new HashSet<string>();

            int transitionNumber = 0;
            foreach (var transition in transitions)
            {
                FillActiveClockBounds(clockBounds, transition.EndState, transition.ActionBranches);

                string transitionName = $"{startState}.{transition.EndState}({++transitionNumber})";
                foreach (string clock in clockBounds[transition.EndState])
                    if (clock != transition.EndState)
                        clockBounds[transitionName].Add(clock);

                startStateActiveClocks.UnionWith(clockBounds[transitionName]);
            }

            if (clockBounds.TryGetValue(startState, out var startStateClocks))
                startStateClocks.UnionWith(startStateActiveClocks);
            else
                clockBounds[startState] = startStateActiveClocks;
        }

        #endregion


        #region Unused visitors

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(ControllerNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(LinkNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(ReferenceNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(EventDeclNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(ActionGraphTreeRootNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(ActionTransitionNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(CommunicatorNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(MachineNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(InvocationGraphTreeRootNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(InvocationTransitionNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(DTONode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(CLSNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(PrimitiveTypeNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(CollectionTypeNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(TupleTypeNode node)
        {
            throw new NotImplementedException();
        }

        public Dictionary<EntityNode, Dictionary<string, HashSet<string>>> Visit(UserDefTypeNode node)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
