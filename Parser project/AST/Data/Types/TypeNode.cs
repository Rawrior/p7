﻿using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Data.Types
{
    public abstract class TypeNode : IASTNode
    {
        public abstract T Accept<T>(IASTVisitor<T> visitor);

        public abstract bool Compare(TypeNode type);
    }
}
