﻿using System;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST
{
    public interface IASTNode
    {
        T Accept<T>(IASTVisitor<T> visitor);
    }
}
