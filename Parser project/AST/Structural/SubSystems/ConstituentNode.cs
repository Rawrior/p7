﻿using System;
using System.Collections.Generic;
using XMLTester.SemanticAnalysis;

namespace XMLTester.AST.Structural.SubSystems
{
    public class ConstituentNode : SubSystemNode
    {
        public IReadOnlyList<string> BoundaryEntityIDs { get; }

        public IReadOnlyList<SubSystemNode> SubSystems { get; }

        public IReadOnlyList<LinkNode> Links { get; }


        public ConstituentNode(string papID, string id, int count, IReadOnlyList<string> boundaryEntityIDs, IReadOnlyList<SubSystemNode> subSystems, IReadOnlyList<LinkNode> links)
            : base(papID, id, count)
        {
            BoundaryEntityIDs = boundaryEntityIDs;
            SubSystems = subSystems;
            Links = links;
        }


        public override T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }
    }
}
