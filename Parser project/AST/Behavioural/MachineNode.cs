﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using XMLTester.AST.Data.Types;
using XMLTester.Code_Gen;
using XMLTester.SemanticAnalysis;
using XMLTester.SymbolTables;

namespace XMLTester.AST.Behavioural
{
    public class MachineNode : IASTNode
    {
        public string PapID { get; }

        public IReadOnlyList<TypeNode> InputTypes { get; set; }
        public IReadOnlyList<TypeNode> OutputTypes { get; set; }

        public string MachineName { get; }

        public IReadOnlyList<string> States { get; }

        public IReadOnlyList<InvocationGraphTreeRootNode> GraphTreeRoots { get; }

        public IReadOnlyList<string> MachineCalls { get; }
        public List<string> HandlerCalls { get; }

        public Dictionary<string, (List<string> input, List<string> output)> HandlerCallsWithVariables { get; }

        public SymbolTable Scope { get; set; }

        public MachineNode(string id, string name, IReadOnlyList<string> states, IReadOnlyList<InvocationGraphTreeRootNode> graphTreeRoots)
        {
            PapID = id;
            MachineName = name;
            States = states;
            GraphTreeRoots = graphTreeRoots;
            MachineCalls =
                GraphTreeRoots
                .SelectMany(root => GetNamesOfCalledMachines(root.InvokationBranches))
                .ToList();
            HandlerCalls =
                GraphTreeRoots
                .SelectMany(root => GetHandlerCalls(root.InvokationBranches))
                .ToList();
            HandlerCallsWithVariables =
                GraphTreeRoots
                .SelectMany(root => GetHandlerCallsWithVariables(root.InvokationBranches))
                .ToDictionary(key => key.Key, value => value.Value);
        }

        public T Accept<T>(IASTVisitor<T> visitor)
        {
            return visitor.Visit(this);
        }


        private List<string> GetNamesOfCalledMachines(IEnumerable<InvocationTransitionNode> invokations)
        {
            List<string> machineCalls = new List<string>();
            foreach (InvocationTransitionNode invokation in invokations)
            {
                if (invokation.InvokationOperation is InvokeMachineOperation opr)
                    machineCalls.Add(opr.InvokationName);
                machineCalls.AddRange(GetNamesOfCalledMachines(invokation.InvokationBranches));
            }
            return machineCalls;
        }

        private List<string> GetHandlerCalls(IEnumerable<InvocationTransitionNode> invokations)
        {
            List<string> handlerCalls = new();
            foreach (InvocationTransitionNode invokation in invokations)
            {
                if (invokation.InvokationOperation is InvokeEventOperation opr)
                {
                    string handler = opr.HandlerName;
                    if (!handlerCalls.Contains(handler))
                        handlerCalls.Add(handler);
                }

                handlerCalls.AddRange(GetHandlerCalls(invokation.InvokationBranches));
            }
            return handlerCalls;
        }

        private Dictionary<string, (List<string> input, List<string> output)> GetHandlerCallsWithVariables(IReadOnlyList<InvocationTransitionNode> invokationBranches)
        {

            Dictionary<string, (List<string> input, List<string> output)> handlerCalls = new();
            List<Dictionary<string, (List<string> input, List<string> output)>> dictionaries = new();
            foreach (InvocationTransitionNode invokation in invokationBranches)
            {
                if (invokation.InvokationOperation is InvokeEventOperation opr)
                {
                    string handler = opr.HandlerName;
                    if (!handlerCalls.ContainsKey(handler))
                    {
                        List<string> inputs = new();
                        List<string> outputs = new();
                        foreach (string input in invokation.InputVariables)
                        {
                            inputs.Add(input);
                        }
                        foreach (string output in invokation.OutputVariables)
                        {
                            outputs.Add(output);
                        }
                        handlerCalls.Add(handler, (inputs, outputs));
                    }
                }

                dictionaries.Add(GetHandlerCallsWithVariables(invokation.InvokationBranches));
            }
            foreach (var dic in dictionaries)
            {
                foreach (var kvp in dic)
                {
                    if (!handlerCalls.ContainsKey(kvp.Key))
                    {
                        handlerCalls.Add(kvp.Key, kvp.Value);
                    }
                    //Don't need to do anything else. If the same handler is used multiple times, then it does not matter,
                    // as  we assume type-correctness, so whether it be one variable or another is immaterial.
                }
            }
            return handlerCalls;
        }
    }
}
