using System;
using System.Collections.Generic;
using System.Linq;
using XMLTester.AST;
using XMLTester.AST.Behavioural;
using XMLTester.AST.Data;
using XMLTester.AST.Data.Types;
using XMLTester.AST.Structural;
using XMLTester.AST.Structural.SubSystems;
using XMLTester.SymbolTables;

namespace XMLTester.SemanticAnalysis
{
    class TypePropagator : IASTVisitor<bool>
    {
        public bool Visit(ModelNode node)
        {
            bool didPropagate = false;
            foreach (ModelNode modelnode in node.Dependencies)
            {
                if (modelnode.Accept(this))
                    didPropagate = true;
            }


            return didPropagate || node.MainSoS.Accept(this);
        }

        public bool Visit(SoSNode node)
        {
            bool didPropagate = false;
            foreach(SubSystemNode subsystem in node.SubSystems)
            {
                if (subsystem.Accept(this))
                {
                    didPropagate = true;
                }
            }
            return didPropagate;
        }

        public bool Visit(LinkNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(ConstituentNode node)
        {
            bool didPropagate = false;
            foreach(SubSystemNode subsystem in node.SubSystems)
            {
                if (subsystem.Accept(this))
                {
                    didPropagate = true;
                }
            }
            return didPropagate;
        }

        public bool Visit(EntityNode node)
        {
            bool didPropagate = false;
            List<object> dtos = new List<object>(node.DTOs);
            dtos.AddRange(node.Scope.PropagatedSymbols);

            foreach (object DTO in dtos)
            {
                DTONode dtonode = null;
                bool isPropagated = false;
                if (DTO is DTONode)
                    dtonode = DTO as DTONode;
                else if (DTO is Propagation prop) {
                    dtonode = prop.Symbol.ASTNode as DTONode;
                    isPropagated = true;
                }

                foreach(KeyValuePair<string, SymbolTable> target in node.Scope.Parent.Scopes)
                {
                    //Look for propagation sources for the DTO
                    if (isPropagated)
                    {
                        if((DTO as Propagation).PropagationSources.Contains(target.Value))
                        {
                            continue;
                        }
                    }
                    if (target.Value.ASTNode == node)
                        continue;
                    if (target.Value.ASTNode is EntityNode ent)
                    {
                        if (Propagate(node, ent, DTO))
                            didPropagate = true;
                    }
                    else if (target.Value.ASTNode is ConstituentNode con)
                    {
                        foreach (EntityNode entity in con.SubSystems.Where(x => x is EntityNode && con.BoundaryEntityIDs.Contains(x.ID)))
                        {
                            if (Propagate(node, entity, DTO))
                                didPropagate = true;
                        }
                    }
                    else if(target.Value.ASTNode is SoSNode sos)
                    {
                        continue;
                    }
                    else
                        throw new InvalidOperationException("Unexpected Scopetype encountered.");

                }
                if(CheckIfBoundary(node))
                    foreach(var target in node.Scope.Parent.Parent.Scopes)
                    {
                        if (isPropagated)
                        {
                            if ((DTO as Propagation).PropagationSources.Contains(target.Value))
                            {
                                continue;
                            }
                        }
                        if (target.Value.ASTNode is EntityNode ent)
                        {
                            if (Propagate(node, ent, DTO))
                                didPropagate = true;
                        }
                        else if (target.Value.ASTNode is ConstituentNode con)
                        {
                            foreach (EntityNode entity in con.SubSystems.Where(x => x is EntityNode && x != node && con.BoundaryEntityIDs.Contains(x.ID)))
                            {
                                if(Propagate(node, entity, DTO))
                                    didPropagate = true;
                            }
                        }
                        else if(target.Value.ASTNode is SoSNode sos)
                        {
                            //TODO
                        }
                        else 
                            throw new InvalidOperationException("Unexpected Scopetype encountered.");
                    }
            }

            return didPropagate;
        }

        private bool Propagate(EntityNode propagator, EntityNode target, object DTO)
        {
            bool didPropagate = false;
            IEnumerable<DTONode> propagatorDTOs = propagator.DTOs.Concat(propagator.Scope.PropagatedSymbols.Select(p => p.Symbol.ASTNode as DTONode));
            DTONode dtonode;
            if (DTO is Propagation DTOpropagation)
            {
                dtonode = DTOpropagation.Symbol.ASTNode as DTONode;
            }
            else
                dtonode = DTO as DTONode;

            //check if target already has dto, and if so, update propagation sources.
            var prop = target.Scope.PropagatedSymbols.Where(prop => prop.Symbol.ASTNode == dtonode).ToList();
            if (prop.Any())
            {
                if (prop.SingleOrDefault() == null)
                    throw new InvalidOperationException("Scope contained multiple propagation entries with dto " + dtonode.ID);
                if (prop.SingleOrDefault().PropagationSources.Contains(propagator.Scope))
                    return false;
                prop[0].PropagationSources.Add(propagator.Scope);
                return false;
            }

            #region base and field
            //Check if we need a baseDTO we don't already have, and if any of our dto's fields use another dto that we don't have.
            IEnumerable<DTONode> targetDTOs = target.DTOs.Concat(target.Scope.PropagatedSymbols.Select(x => x.Symbol.ASTNode as DTONode));
            Queue<DTONode> dtoqueue = new();
            foreach (var targetDTO in targetDTOs)
                dtoqueue.Enqueue(targetDTO);
            while(dtoqueue.Count > 0)
            {
                DTONode targetDTO = dtoqueue.Dequeue();
                bool isBase = targetDTO.Base == (propagator.Scope.GetSymbol(dtonode.ID) ?? propagator.Scope.GetPropagatedSymbol(dtonode.ID)).GetFullyQualifiedName();
                bool doesNotHaveBase = !targetDTOs.Contains(dtonode);
                if (isBase && doesNotHaveBase)
                {
                    Symbol basesym = propagator.Scope.GetSymbol(dtonode.ID);
                    target.Scope.PropagateSymbol(basesym, new List<SymbolTable> { propagator.Scope });
                    dtoqueue.Enqueue(basesym.ASTNode as DTONode);
                    didPropagate = true;
                }
                //CheckFieldsForDTO()
                Queue<TypeNode> queue = new();
                foreach (Field field in targetDTO.Fields)
                {
                    queue.Enqueue(field.Type);
                    while (queue.Count > 0)
                    {
                        TypeNode fieldtype = queue.Dequeue();
                        if (fieldtype is PrimitiveTypeNode)
                            continue;
                        else if (fieldtype is CollectionTypeNode collec)
                        {
                            queue.Enqueue(collec.ElementType);
                        }
                        else if (fieldtype is TupleTypeNode tuple)
                        {
                            foreach (TypeNode elem in tuple.ElementTypes)
                                queue.Enqueue(elem);
                        }
                        else if (fieldtype is UserDefTypeNode usrtype)
                        {
                            if (targetDTOs.Where(t => t.ID == usrtype.UserTypeID).SingleOrDefault() == null)
                            {
                                var dtosyms = propagatorDTOs.Select(p => propagator.Scope.GetSymbol(p.ID)).Where(p => p != null);
                                var propdtos = dtosyms.Where(p => p.GetFullyQualifiedName() == usrtype.UserTypeID).SingleOrDefault();
                                if(propdtos != null)
                                {
                                    target.Scope.PropagateSymbol(propagator.Scope.GetSymbol(dtonode.ID), new List<SymbolTable> { propagator.Scope });
                                    didPropagate = true;
                                    DTONode node = propdtos.ASTNode as DTONode;
                                    dtoqueue.Enqueue(node);
                                    foreach(Field f in node.Fields)
                                    {
                                        queue.Enqueue(f.Type);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            #region Outward Propagation
            //Outward propagation rules:
            //Point 1: Propagator and target must have a shared parent
                //trivially covered by only selecting targets that have shared parents or are boundary entities
            // on constituents with shared parent.
            //Point 2: the DTO must be on the propagator
                //Trivially covered by looping through all propagator dtos.
            //Point 3: 
            //point 3.1: The propagator must expose the DTO through the input or output of a handler H.
            IEnumerable<EventDeclNode> propagatorHandlerDecls =
                propagator.Controller.EventDeclarations.Where(ev
                => ev.InputTypes.Concat(ev.OutputTypes).Any(type
                    => type is UserDefTypeNode
                       && (type as UserDefTypeNode).UserTypeID == dtonode.ID));
            //TODO handle nested types
            if (propagatorHandlerDecls.Any())
            {
                //point 3.2: The propagator must expose the handler in an action transition.
                //Assumes that handler is actually exposed, dood

                //Point 4: The target must be able to initiate communication with the propagator
                //First, we find any shared links
                IEnumerable<string> OutwardSharedLinks = target.Communicators.Where(
                    com => propagator.Communicators.Any(propcom => propcom.LinkID == com.LinkID)
                ).Select(com => com.LinkID);

                //The target must be able to initiate communication on the link
                IEnumerable<string> OutwardInitiatingSharedLinks =
                    target.Scope.Parent.Symbols.Concat(propagator.Scope.Parent.Symbols).Where(x =>
                        x.Value.ASTNode is LinkNode
                        && OutwardSharedLinks.Contains((x.Value.ASTNode as LinkNode).ID)
                        && (x.Value.ASTNode as LinkNode).Connections.Any(y =>
                            y.EntityID.Split(".").Last() == target.ID
                            && y.ConnectionType == ConnectionTypes.Transceiver
                        )
                    ).Select(x => (x.Value.ASTNode as LinkNode).ID);
                //If there are no usabble links, then we cannot propagate
                if (OutwardInitiatingSharedLinks.Any())
                {
                    //If any shared links can be used, then we loop through each.
                    foreach (CommunicatorNode com in target.Communicators.Where(x => OutwardInitiatingSharedLinks.Contains(x.LinkID)))
                    {
                        //Point 5: There is a communicator in the target that contains a handler which uses the DTO
                        var test = com.GetHandlerCalls();
                        if (com.GetHandlerCalls().Any(handler => propagatorHandlerDecls.Any(h => h.Name == handler)))
                        {
                            //We can now propagate
                            Symbol DTOSymbol;
                            if (DTO is Propagation propagation)
                                DTOSymbol = propagation.Symbol;
                            else
                                DTOSymbol = propagator.Scope.GetSymbol(dtonode.ID);
                            target.Scope.PropagateSymbol(DTOSymbol, new List<SymbolTable> { propagator.Scope });
                            return true;
                        }
                    }
                }

            }
            #endregion
            #region Inward propagation check
            //Inwards propagation rules::
            //Point 1: Propagator and target must have a shared parent
                //trivially covered by only selecting targets that have shared parents or are boundary entities
            // on constituents with shared parent.
            //Point 2: the DTO must be on the propagator
                //Trivially covered by looping through all propagator dtos.
            //Point 3: The propagator must be able to initiate communication with the target through some link
                //Find all shared links, and then find all that the propagator is a transceiver on
            IEnumerable<string> InwardSharedLinks = target.Communicators.Where(
                        com => propagator.Communicators.Any(propcom => propcom.LinkID == com.LinkID)
                    ).Select(com => com.LinkID);

            IEnumerable<string> InwardInitiatingSharedLinks =
                target.Scope.Parent.Symbols.Concat(propagator.Scope.Parent.Symbols).Where(x =>
                    x.Value.ASTNode is LinkNode
                    && InwardSharedLinks.Contains((x.Value.ASTNode as LinkNode).ID)
                    && (x.Value.ASTNode as LinkNode).Connections.Any(y =>
                        y.EntityID.Split(".").Last() == propagator.ID
                        && y.ConnectionType == ConnectionTypes.Transceiver
                    )
                ).Select(x => (x.Value.ASTNode as LinkNode).ID);
            //if no initiating links exist, then there is no propagation
            if(InwardInitiatingSharedLinks.Any())
            {
                //Point 4
                //Point 4.1: The propagator must have a communicator, which initiator is reachable, that invokes
                // some H from the target.
                //Point 4.2: The handler H must take the DTO as input.
                IEnumerable<string> targetHandler =
                    target.Controller.EventDeclarations.Where(ev
                        => ev.EventType == EventTypes.Initiator
                        &&ev.InputTypes.Any(type
                            => type is UserDefTypeNode
                            && (type as UserDefTypeNode).UserTypeID == dtonode.ID
                       )
                ).Select(handler => handler.Name);

                //If there are no usabble links, then we cannot propagate
                if (InwardInitiatingSharedLinks.Any())
                {
                    //If any shared links can be used, then we loop through each.
                    foreach (CommunicatorNode com in target.Communicators.Where(x => InwardInitiatingSharedLinks.Contains(x.LinkID)))
                    {
                        //Point 5: There is a communicator in the target that contains a handler which uses the DTO
                        if (com.GetHandlerCalls().Any(handler => targetHandler.Contains(handler)))
                        {
                            //We can now propagate
                            Symbol DTOSymbol;
                            if (DTO is Propagation propagation)
                                DTOSymbol = propagation.Symbol;
                            else
                                DTOSymbol = propagator.Scope.GetSymbol(dtonode.ID);
                            target.Scope.PropagateSymbol(DTOSymbol, new List<SymbolTable> { propagator.Scope });
                            didPropagate = true;
                        }
                    }
                }

            }
            #endregion
            return didPropagate;
        }

        private bool CheckIfBoundary(EntityNode node)
        {
            if (node.Scope.Parent.ASTNode is ConstituentNode con)
            {
                return con.BoundaryEntityIDs.Contains(node.ID);
            }
            else if (node.Scope.Parent.ASTNode is SoSNode sos)
            {
                return sos.BoundaryEntityIDs.Contains(node.ID);
            }
            else
                throw new InvalidOperationException("Unknown encapsulating entity.");
        }

        public bool Visit(ReferenceNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(ControllerNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(EventDeclNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(ActionGraphTreeRootNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(ActionTransitionNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(CommunicatorNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(MachineNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(InvocationGraphTreeRootNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(InvocationTransitionNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(DTONode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(CLSNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(PrimitiveTypeNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(CollectionTypeNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(TupleTypeNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }

        public bool Visit(UserDefTypeNode node)
        {
            throw new NotImplementedException("Should not be visitable");
        }
    }
}
