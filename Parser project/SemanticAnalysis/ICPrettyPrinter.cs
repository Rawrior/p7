﻿using System;
using System.Collections.Generic;
using System.Text;
using XMLTester.IntermediateNodes;

namespace XMLTester.SemanticAnalysis
{
    class ICPrettyPrinter : IIntermediateVisitor<String>
    {
        private int indentCount = 0;
        private readonly int indentDepth = 2;

        public string Print(InternalClassNode node)
        {
            return VisitInternalClassNode(node);
        }

        private String Indents()
        {
            String s = "";
            for (int i = 0; i < indentCount; i++)
                s += new string(' ', indentDepth);
            return s;
        }

        public string VisitInternalClassNode(InternalClassNode node)
        {
            StringBuilder sb = new StringBuilder();
            
            //sb.Append("DiagramNode ").Append(node.ID).Append("\n");
            indentCount++;
            foreach(INode element in node.Elements) { sb.Append(Indents()).Append(element.Accept(this)); };
            indentCount--;
            
            return sb.ToString();
        }

        public string VisitAssociationNode(AssociationNode node)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("AssociationNode ").Append(node.ID).Append('\n');
            indentCount++;
            foreach(ConstraintNode constraint in node.Constraints) { sb.Append(Indents()).Append(constraint.Accept(this)); };
            indentCount--;

            return sb.ToString();
        }

        public string VisitClassNode(ClassNode node)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("ClassNode ").Append(node.PapID).Append('\n');
            indentCount++;
            foreach(PropertyNode variable in node.Variables) { sb.Append(Indents()).Append(variable.Accept(this)); };
            foreach(OperationNode method in node.Methods) { sb.Append(Indents()).Append(method.Accept(this)); };
            foreach(ConstraintNode constraint in node.Constraints) { sb.Append(Indents()).Append(constraint.Accept(this)); };
            indentCount--;

            return sb.ToString();
        }

        public string VisitConstraintNode(ConstraintNode node)
        {
            return $"ConstraintNode {node.ID}\n";
        }

        public string VisitOperationNode(OperationNode node)
        {
            return $"OperationNode {node.PapID}\n";
        }

        public string VisitPackagedElementNode(PackagedElementNode _)
        {
            return $"PackagedElementNode\n";
        }

        public string VisitParameterNode(ParameterNode node)
        {
            return $"ParameterNode {node.PapID}\n";
        }

        public string VisitPropertyNode(PropertyNode node)
        {
            return $"PropertyNode {node.PapID}\n";
        }

        public string VisitTypedNode(TypedNode node)
        {
            return "TypedNode\n";
        }

        public string VisitConnectionEndNode(ConnectionEndNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitFinalStateNode(FinalStateNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitInitialStateNode(InitialStateNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitMachineNode(MachineNode node)
        {
            throw new NotImplementedException();
        }

        //public string VisitModelNode(ModelNode node)
        //{
        //    throw new NotImplementedException();
        //}

        //public string VisitRegionNode(RegionNode node)
        //{
        //    throw new NotImplementedException();
        //}

        //public string VisitStateChartMachineNode(StateChartMachineNode node)
        //{
        //    throw new NotImplementedException();
        //}

        public string VisitStateNode(StateNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitTransitionNode(TransitionNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitEntityBehaviorNode(EntityBehavior node)
        {
            throw new NotImplementedException();
        }

        public string VisitComponentDiagramNode(ComponentDiagramNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitComponentNode(ComponentNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitConnectorNode(ConnectorNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitAttributeNode(AttributeNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitDTONode(IntermediateDTONode node)
        {
            throw new NotImplementedException();
        }

        public string VisitTypeNode(IntermediateTypeNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitSoSNode(IntermediateSoSNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitEntityNode(EntityNode node)
        {
            throw new NotImplementedException();
        }

        public string VisitControllerNode(ControllerMachineNode node)
        {
            throw new NotImplementedException();
        }
    }
}
