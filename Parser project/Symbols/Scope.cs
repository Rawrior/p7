﻿using System;
using System.Collections.Generic;

namespace XMLTester.Symbols
{
    public class Scope
    {
        public string Name { get; set; }
        Scope Parent { get; set; }

        Dictionary<string, Symbol> symbols;

        public Scope(string name, Scope scope)
        {
            Name = name;
            Parent = scope;
            symbols = new Dictionary<string, Symbol>();
        }

        public void AddSymbol(Symbol symbol)
        {
            if (symbols.ContainsKey(symbol.Name))
                throw new InvalidOperationException($"Tried to redefine a variable <{symbol}> in the same scope");

            symbols.Add(symbol.Name, symbol);
        }

        public Boolean ContainsSymbol(string Name)
        {
            return symbols.ContainsKey(Name);
        }

        public Symbol GetSymbolWithName(string Name)
        {
            if (!symbols.ContainsKey(Name))
                throw new InvalidOperationException(
                    "Symbol was attempted to be accessed but did not exist in scope\nName: " + Name + "\nScope: " + this);

            return symbols[Name];
        }

        public override string ToString()
        {
            return String.Format("{0, -10} --|> {1, -10} ({2:D} symbols) : {3}",
                Name,
                Parent != null ? Parent.Name : "",
                symbols.Count,
                symbols.ToString());
        }
    }
}
