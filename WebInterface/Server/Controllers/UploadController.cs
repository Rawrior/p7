﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebInterface.Server.Controllers
{
    [ApiController]
    [Route("api")]
    public class UploadController : Controller
    {
        [HttpPost("upload")]
        public IActionResult UploadFile()
        {
            Console.WriteLine("Entered upload");
            IFormFile file = Request.Form.Files.First();
            Stream stream = file.OpenReadStream();
            Directory.CreateDirectory("resources");
            FileStream fileStream = System.IO.File.Create("resources/model.uml");
            stream.CopyTo(fileStream);
            stream.Close();
            fileStream.Close();
            Console.WriteLine("Leaving upload");
            return Ok();
        }

        [HttpGet("model")]
        public IActionResult CompileModel()
        {
            Console.WriteLine("Gonna do a thing!");
            var file = new FileInfo("resources/model.uml");
            Console.WriteLine("Did a thing!");
            bool result = XMLTester.Program.ACE_It_Up(new string[] { file.FullName, "", "../../openapi-generator-cli-4.3.1.jar"});
            Console.WriteLine("Finished ACE");
            if (result)
                return Ok();
            else
                return BadRequest();
        }
    }
}
